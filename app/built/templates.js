angular.module('templates-main', ['partials/home.html', 'partials/logout.html', 'partials/project-edit.html', 'partials/project-view.html', 'partials/projects.html', 'partials/register.html', 'partials/ticket-view.html', 'partials/tickets.html', 'partials/version-history.html']);

angular.module("partials/home.html", []).run(["$templateCache", function($templateCache) {
	"use strict";
	$templateCache.put("partials/home.html",
		"<p ></p>\n" +
		"\n" +
		"<div class=\"panel panel-primary\" >\n" +
		"    <div class=\"panel-heading\">Overview</div>\n" +
		"    <div class=\"panel-body\">\n" +
		"        \n" +
		"        <p >Ticktack is a personal project to help me learn new skills and hone my existing skills.</p>\n" +
		"        \n" +
		"        <p >This is a SPA (Single Page Application), written in AngularJS. There is also an <a href=\"http://ticktack.pedanticantic.click/\" target=\"ticktack_laravel\" >MVC version written in Laravel</a>. The data and users are the same between the two applications.</p>\n" +
		"        \n" +
		"        <p >Click here for the <a href=\"https://github.com/pedanticantic/ticktack.ng\" target=\"spa_github\" >source code for this version</a>.</p>\n" +
		"        \n" +
		"    </div>\n" +
		"</div>\n" +
		"\n" +
		"<div class=\"panel panel-info\" >\n" +
		"    <div class=\"panel-heading\">Registration and Logging in</div>\n" +
		"    <div class=\"panel-body\">\n" +
		"        \n" +
		"        <p >You can register through either application (<a href=\"http://ticktack.pedanticantic.click/\" target=\"ticktack_laravel\" >the Laravel MVC one</a> or this one). The user accounts are shared between them.</p>\n" +
		"        \n" +
		"        <p >To log in (on this application), just start trying to use it, eg click on Projects. If you are not currently logged in, a login pop-up will appear. You have to enter valid account details before you can go to any pages containing data.</p>\n" +
		"        \n" +
		"    </div>\n" +
		"</div>\n" +
		"");
}]);

angular.module("partials/logout.html", []).run(["$templateCache", function($templateCache) {
	"use strict";
	$templateCache.put("partials/logout.html",
		"<h3 >Log out</h3>\n" +
		"\n" +
		"<p >You are now logged out.</p>\n" +
		"");
}]);

angular.module("partials/project-edit.html", []).run(["$templateCache", function($templateCache) {
	"use strict";
	$templateCache.put("partials/project-edit.html",
		"<h2 >{{ getModeDesc() }} Project</h2>\n" +
		"\n" +
		"<form name=\"editProject\" ng-submit=\"saveProject()\" >\n" +
		"    <table >\n" +
		"        <tbody >\n" +
		"            <tr >\n" +
		"                <th >Name *</th>\n" +
		"                <td >\n" +
		"                    <input type=\"text\" name=\"project_name\" data-ng-model=\"project.name\" data-ng-required=\"true\" data-ng-minlength=\"5\" data-ng-maxlength=\"30\" />\n" +
		"                    (Between 5 and 30 characters)\n" +
		"                    <p data-ng-hide=\"editProject.project_name.$valid\" class=\"error_block\" >Name is not valid</p>\n" +
		"                </td>\n" +
		"            </tr>\n" +
		"            <tr >\n" +
		"                <th >Description *</th>\n" +
		"                <td >\n" +
		"                    <textarea data-ng-model=\"project.description\" rows=\"8\" cols=\"50\" data-ng-required=\"true\" ></textarea>\n" +
		"                </td>\n" +
		"            </tr>\n" +
		"        </tbody>\n" +
		"    </table>\n" +
		"    \n" +
		"    <p ><input class=\"btn btn-primary\" type=\"submit\" value=\"Save\" /></p>\n" +
		"</form>\n" +
		"\n" +
		"<br />\n" +
		"<p >Go to <a ui-sref=\"projects()\" >Project List</a></p");
}]);

angular.module("partials/project-view.html", []).run(["$templateCache", function($templateCache) {
	"use strict";
	$templateCache.put("partials/project-view.html",
		"<h2 >View Project</h2>\n" +
		"\n" +
		"<h3 >{{ project.name }}</h3>\n" +
		"\n" +
		"<div class=\"formatted-block\" data-ng-bind-html=\"project.descriptionAsMarkdown | sanitise\" >\n" +
		"</div>\n" +
		"\n" +
		"<br />\n" +
		"<p ><a ui-sref=\"projectedit({id: project.id})\" >Edit</a> this project</p>\n" +
		"\n" +
		"<p >Go to <a ui-sref=\"projects()\" >Project List</a></p>\n" +
		"\n" +
		"<h4 >Tickets in this project</h4>\n" +
		"\n" +
		"<div class=\"project-status-panel\" >\n" +
		"    <div class=\"panel panel-info\">\n" +
		"        <div class=\"panel-heading\">\n" +
		"            <h3 class=\"panel-title\">By Status</h3>\n" +
		"        </div>\n" +
		"        <div class=\"panel-body\">\n" +
		"            <ul class=\"list-group\">\n" +
		"                <li data-ng-repeat=\"ticketStatus in ticketStatuses\">\n" +
		"                    <a ui-sref=\"tickets(\n" +
		"{\n" +
		"    projectId: project.id,\n" +
		"    statuses: ticketStatus.code\n" +
		"})\" >\n" +
		"                        {{ countTicketFilter({status: ticketStatus.code}) }} {{ ticketStatus.desc }}\n" +
		"                    </a>\n" +
		"                </li>\n" +
		"            </ul>\n" +
		"        </div>\n" +
		"    </div>\n" +
		"</div>\n" +
		"\n" +
		"<div class=\"project-status-panel\" >\n" +
		"    <div class=\"panel panel-info\">\n" +
		"        <div class=\"panel-heading\">\n" +
		"            <h3 class=\"panel-title\">Open Tickets By Priority</h3>\n" +
		"        </div>\n" +
		"        <div class=\"panel-body\">\n" +
		"            <ul class=\"list-group\">\n" +
		"                <li data-ng-repeat=\"ticketPriority in ticketPriorities\">\n" +
		"                    <a ui-sref=\"tickets(\n" +
		"{\n" +
		"    projectId: project.id,\n" +
		"    statuses: ['new', 'assigned', 'in progress'],\n" +
		"    priorities: ticketPriority.code\n" +
		"})\" >\n" +
		"                        {{ countTicketFilter({priority: ticketPriority.code, status: ['new', 'assigned', 'in progress']}) }} {{ ticketPriority.desc }}\n" +
		"                    </a>\n" +
		"                </li>\n" +
		"            </ul>\n" +
		"        </div>\n" +
		"    </div>\n" +
		"</div>\n" +
		"\n" +
		"<div class=\"project-status-panel\" >\n" +
		"    <div class=\"panel panel-info\">\n" +
		"        <div class=\"panel-heading\">\n" +
		"            <h3 class=\"panel-title\">Your Open Tickets By Assignment Type</h3>\n" +
		"        </div>\n" +
		"        <div class=\"panel-body\">\n" +
		"            <ul class=\"list-group\">\n" +
		"                <li >\n" +
		"                    <a ui-sref=\"tickets(\n" +
		"{\n" +
		"    projectId: project.id,\n" +
		"    statuses: ['new', 'assigned', 'in progress'],\n" +
		"    assignmentUserIds: getLoggedInUserId(),\n" +
		"    assignmentLevel: 'primary'\n" +
		"})\" >\n" +
		"                        {{ countTicketFilter({status: ['new', 'assigned', 'in progress'], assignments: {userId: getLoggedInUserId(), sortOrder: 1} }) }} Primary\n" +
		"                    </a>\n" +
		"                </li>\n" +
		"                <li >\n" +
		"                    <a ui-sref=\"tickets(\n" +
		"{\n" +
		"    projectId: project.id,\n" +
		"    statuses: ['new', 'assigned', 'in progress'],\n" +
		"    assignmentUserIds: getLoggedInUserId(),\n" +
		"    assignmentLevel: 'other'\n" +
		"})\" >\n" +
		"                        {{ countTicketFilter({status: ['new', 'assigned', 'in progress'], assignments: {userId: getLoggedInUserId(), sortOrder: '!1'} }) }} Other\n" +
		"                    </a>\n" +
		"                </li>\n" +
		"                <li >\n" +
		"                    <a ui-sref=\"tickets(\n" +
		"{\n" +
		"    projectId: project.id,\n" +
		"    statuses: ['new', 'assigned', 'in progress'],\n" +
		"    assignmentUserIds: getLoggedInUserId()\n" +
		"})\" >\n" +
		"                        {{ countTicketFilter({status: ['new', 'assigned', 'in progress'], assignments: {userId: getLoggedInUserId()} }) }} Any\n" +
		"                    </a>\n" +
		"                </li>\n" +
		"            </ul>\n" +
		"        </div>\n" +
		"    </div>\n" +
		"</div>\n" +
		"");
}]);

angular.module("partials/projects.html", []).run(["$templateCache", function($templateCache) {
	"use strict";
	$templateCache.put("partials/projects.html",
		"<h2 >Projects</h2>\n" +
		"\n" +
		"<p >Add a <a ui-sref=\"projectcreate()\" >new project</a>.</p>\n" +
		"\n" +
		"<p >\n" +
		"    <form data-ng-submit=\"getProjects()\" >\n" +
		"        Temporary quick filter:\n" +
		"        <input type=\"text\" name=\"project_filter\" data-ng-model=\"projectFilter\"/>\n" +
		"        <input class=\"btn btn-primary\" type=\"submit\" value=\"Go\" />\n" +
		"    </form>\n" +
		"</p>\n" +
		"\n" +
		"<p data-ng-show=\"isLoading()\" ><img src=\"images/spinner.gif\" /> Loading...</p>\n" +
		"<span data-ng-hide=\"isLoading()\" >\n" +
		"    <p data-ng-show=\"projects.length == 0\" >No projects match your criteria.</p>\n" +
		"    <h3 data-ng-show=\"projects.length > 0\" >{{ projects.length }} projects...</h3>\n" +
		"    <table class=\"app-data striped\" data-ng-show=\"projects.length > 0\">\n" +
		"        <th >Id</th><th >Name</th><th >Description</th><th colspan=\"3\">Options</th>\n" +
		"        <tr data-ng-repeat=\"project in projects\" >\n" +
		"            <td >{{ project.id }}</td>\n" +
		"            <td >{{ project.name }}</td>\n" +
		"            <td class=\"description\" data-ng-bind-html=\"project.descriptionAsMarkdown | sanitise\" ></td>\n" +
		"            <td ><a ui-sref=\"projectview({id: project.id})\" >View</a></td>\n" +
		"            <td ><a ui-sref=\"projectedit({id: project.id})\" >Edit</a></td>\n" +
		"            <td ><a ui-sref=\"tickets({projectId: project.id})\" >Tickets</a></td>\n" +
		"        </tr>\n" +
		"    </table>\n" +
		"</span>\n" +
		"");
}]);

angular.module("partials/register.html", []).run(["$templateCache", function($templateCache) {
	"use strict";
	$templateCache.put("partials/register.html",
		"<h2 >Register</h2>\n" +
		"\n" +
		"<p >\n" +
		"    <form data-ng-submit=\"registerUser()\" >\n" +
		"        <table >\n" +
		"            <tbody >\n" +
		"                <tr >\n" +
		"                    <th >Username</th>\n" +
		"                    <td ><input type=\"text\" name=\"username\" data-ng-model=\"username\"/></td>\n" +
		"                </tr>\n" +
		"                <tr >\n" +
		"                    <th >Password</th>\n" +
		"                    <td ><input type=\"password\" name=\"password\" data-ng-model=\"password\"/></td>\n" +
		"                </tr>\n" +
		"                <tr >\n" +
		"                    <th >Confirm Password</th>\n" +
		"                    <td ><input type=\"password\" name=\"confirm_password\" data-ng-model=\"confirm_password\"/></td>\n" +
		"                </tr>\n" +
		"            </tbody>\n" +
		"        </table>\n" +
		"        <input type=\"submit\" value=\"Register\" />\n" +
		"        <div data-ng-show=\"errorMessage.length > 0\" >\n" +
		"            <h3 >Errors</h3>\n" +
		"            <ul style=\"color: red\" >\n" +
		"                <li data-ng-repeat=\"msg in errorMessage\" >{{ msg }}</li>\n" +
		"            </ul>\n" +
		"        </div>\n" +
		"    </form>\n" +
		"</p>\n" +
		"");
}]);

angular.module("partials/ticket-view.html", []).run(["$templateCache", function($templateCache) {
	"use strict";
	$templateCache.put("partials/ticket-view.html",
		"<h2 >View/Edit Ticket <img src=\"/images/spinner.gif\" data-ng-show=\"spinners.ticket > 0\" /></h2>\n" +
		"\n" +
		"<table class=\"app-data\" data-ng-click=\"toggleEdit(true)\" >\n" +
		"    <tbody >\n" +
		"        <tr >\n" +
		"            <th >Project</th>\n" +
		"            <td >\n" +
		"                <span ng-switch on=\"editingTicket\" >\n" +
		"                    <span data-ng-switch-when=\"false\">\n" +
		"                        {{ ticket.project.name }}\n" +
		"                    </span>\n" +
		"                    <span data-ng-switch-when=\"true\">\n" +
		"                        <select data-ng-model=\"ticket.projectId\" data-ng-options=\"project.id as project.name for project in projectData\">\n" +
		"                        </select>\n" +
		"                    </span>\n" +
		"                </span>\n" +
		"            </td>\n" +
		"        </tr>\n" +
		"        <tr >\n" +
		"            <th >Type</th>\n" +
		"            <td >\n" +
		"                <span ng-switch on=\"editingTicket\" >\n" +
		"                    <span data-ng-switch-when=\"false\">\n" +
		"                        {{ codeToDesc('ticket_types', ticket.ticketType) }}\n" +
		"                    </span>\n" +
		"                    <span data-ng-switch-when=\"true\">\n" +
		"                        <select data-ng-model=\"ticket.ticketType\" data-ng-options=\"data.code as data.desc for (index, data) in staticData.types\">\n" +
		"                        </select>\n" +
		"                    </span>\n" +
		"                </span>\n" +
		"            </td>\n" +
		"        </tr>\n" +
		"        <tr >\n" +
		"            <th >Summary</th>\n" +
		"            <td >\n" +
		"                <span ng-switch on=\"editingTicket\" >\n" +
		"                    <span data-ng-switch-when=\"false\">\n" +
		"                        {{ ticket.summary }}\n" +
		"                    </span>\n" +
		"                    <span data-ng-switch-when=\"true\">\n" +
		"                        <input type=\"text\" data-ng-model=\"ticket.summary\" maxlength=\"60\" size=\"60\" />\n" +
		"                    </span>\n" +
		"                </span>\n" +
		"            </td>\n" +
		"        </tr>\n" +
		"        <tr >\n" +
		"            <th >Description</th>\n" +
		"            <td >\n" +
		"                <span ng-switch on=\"editingTicket\" >\n" +
		"                    <span data-ng-switch-when=\"false\" class=\"description\" data-ng-bind-html=\"ticket.descriptionMarkdown | sanitise\">\n" +
		"                    </span>\n" +
		"                    <span data-ng-switch-when=\"true\">\n" +
		"                        <textarea data-ng-model=\"ticket.description\" rows=\"6\" cols=\"60\" ></textarea>\n" +
		"                    </span>\n" +
		"                </span>\n" +
		"            </td>\n" +
		"        </tr>\n" +
		"        <tr >\n" +
		"            <th >Created By</th>\n" +
		"            <td >\n" +
		"                <span ng-switch on=\"editingTicket\" >\n" +
		"                    <span data-ng-switch-when=\"false\">\n" +
		"                        {{ ticket.createdByUserName }}\n" +
		"                    </span>\n" +
		"                    <span data-ng-switch-when=\"true\">\n" +
		"                        <select data-ng-model=\"ticket.createdByUserId\" data-ng-options=\"user.id as user.userName for user in userData\">\n" +
		"                        </select>\n" +
		"                    </span>\n" +
		"                </span>\n" +
		"            </td>\n" +
		"        </tr>\n" +
		"        <tr >\n" +
		"            <th >Priority</th>\n" +
		"            <td >\n" +
		"                <span ng-switch on=\"editingTicket\" >\n" +
		"                    <span data-ng-switch-when=\"false\">\n" +
		"                        {{ codeToDesc('ticket_priorities', ticket.priority) }}\n" +
		"                    </span>\n" +
		"                    <span data-ng-switch-when=\"true\">\n" +
		"                        <select data-ng-model=\"ticket.priority\" data-ng-options=\"data.code as data.desc for (index, data) in staticData.priorities\">\n" +
		"                        </select>\n" +
		"                    </span>\n" +
		"                </span>\n" +
		"            </td>\n" +
		"        </tr>\n" +
		"        <tr >\n" +
		"            <th >Status</th>\n" +
		"            <td >\n" +
		"                <span ng-switch on=\"editingTicket\" >\n" +
		"                    <span data-ng-switch-when=\"false\">\n" +
		"                        {{ codeToDesc('ticket_statuses', ticket.status) }}\n" +
		"                    </span>\n" +
		"                    <span data-ng-switch-when=\"true\">\n" +
		"                        <select data-ng-model=\"ticket.status\" data-ng-options=\"data.code as data.desc for (index, data) in staticData.statuses\">\n" +
		"                        </select>\n" +
		"                    </span>\n" +
		"                </span>\n" +
		"            </td>\n" +
		"        </tr>\n" +
		"        <tr >\n" +
		"            <th >Due Datetime</th>\n" +
		"            <td >\n" +
		"                <span ng-switch on=\"editingTicket\" >\n" +
		"                    <span data-ng-switch-when=\"false\">\n" +
		"                        {{ ticket.dueDatetime | date:'dd/MM/yyyy H:mm' }}\n" +
		"                    </span>\n" +
		"                    <span data-ng-switch-when=\"true\">\n" +
		"                        <input type=\"text\" data-ng-model=\"ticket.dueDatetimeForView\" size=\"20\" /> (Needs a date picker)\n" +
		"                    </span>\n" +
		"                </span>\n" +
		"            </td>\n" +
		"        </tr>\n" +
		"        <tr >\n" +
		"            <th >Estimated Length</th>\n" +
		"            <td >\n" +
		"                <span ng-switch on=\"editingTicket\" >\n" +
		"                    <span data-ng-switch-when=\"false\">\n" +
		"                        {{ ticket.estimatedLengthForView }}\n" +
		"                    </span>\n" +
		"                    <span data-ng-switch-when=\"true\">\n" +
		"                        <input type=\"text\" data-ng-model=\"ticket.estimatedLengthForView\" size=\"10\" />\n" +
		"                    </span>\n" +
		"                </span>\n" +
		"            </th>\n" +
		"        </tr>\n" +
		"    </tbody>\n" +
		"</table>\n" +
		"\n" +
		"<br />\n" +
		"<span ng-switch on=\"editingTicket\" >\n" +
		"    <p data-ng-switch-when=\"false\">\n" +
		"        <button data-ng-click=\"toggleEdit()\" class=\"btn btn-primary\" >Edit</button> this ticket\n" +
		"    </p>\n" +
		"    <div data-ng-switch-when=\"true\">\n" +
		"        <p >\n" +
		"            <button data-ng-click=\"saveTicket()\" class=\"btn btn-primary\" >Save</button>\n" +
		"            <button data-ng-click=\"toggleEdit()\" class=\"btn btn-default\" >Cancel</button>\n" +
		"        </p>\n" +
		"    </div>\n" +
		"</span>\n" +
		"\n" +
		"<p data-ng-hide=\"editingTicket\">Add a <a ui-sref=\"ticketadd()\" >new ticket</a>.</p>\n" +
		"\n" +
		"<p >Go to <a ui-sref=\"tickets()\" >Ticket List</a></p>\n" +
		"\n" +
		"<span data-ng-show=\"ticket_id > 0\" >\n" +
		"\n" +
		"<tabset>\n" +
		"    <tab >\n" +
		"        <tab-heading>\n" +
		"            <h3 >Assignments ({{ ticket.getPrimaryAssigneeName() }}) <img src=\"/images/spinner.gif\" data-ng-show=\"spinners.assignment > 0\" /></h3>\n" +
		"        </tab-heading>\n" +
		"        \n" +
		"        <p data-ng-show=\"ticket.assignments.length == 0\" >There are no assignments</p>\n" +
		"        <table class=\"app-data striped ticket-subrecord\" data-ng-show=\"ticket.assignments.length > 0\" >\n" +
		"            <tbody >\n" +
		"                <tr data-ng-repeat=\"assignment in ticket.assignments\" >\n" +
		"                    <td >{{ assignment.getAssigneeType() }}</td>\n" +
		"                    <td >{{ assignment.userName }}</td>\n" +
		"                    <td data-ng-if=\"ticket.assignments.length > 1\" ><span class=\"assign-up-down-container\"><button data-ng-show=\"$index < (ticket.assignments.length - 1)\" data-ng-click=\"moveAssignment(assignment.id, 'down')\" title=\"Move this assignment down\" class=\"btn btn-warning\" ><span class=\"glyphicon glyphicon-chevron-down\" ></span></button></span></td>\n" +
		"                    <td data-ng-if=\"ticket.assignments.length > 1\" ><span class=\"assign-up-down-container\"><button data-ng-show=\"$index > 0\" data-ng-click=\"moveAssignment(assignment.id, 'up')\" title=\"Move this assignment up\" class=\"btn btn-warning\" ><span class=\"glyphicon glyphicon-chevron-up\" ></span></button></span></td>\n" +
		"                    <td ><button data-ng-click=\"unassign(assignment.id)\" class=\"btn btn-danger\" >Unassign</button>\n" +
		"                </tr>\n" +
		"            </tbody>\n" +
		"        </table>\n" +
		"\n" +
		"        <span ng-switch on=\"adding.assignment\" >\n" +
		"            <p data-ng-switch-when=\"false\">\n" +
		"                <button data-ng-click=\"toggle('assignment')\" class=\"btn btn-primary\" >Add Assignment</button>\n" +
		"            </p>\n" +
		"            <div data-ng-switch-when=\"true\">\n" +
		"                <p >\n" +
		"                    <select data-ng-model=\"newAssignee.id\" data-ng-options=\"assignee.id as assignee.userName for assignee in assignees\">\n" +
		"                    </select>\n" +
		"                </p>\n" +
		"                <p >\n" +
		"                    <button data-ng-click=\"saveAssignment()\" class=\"btn btn-primary\" >Save</button>\n" +
		"                    <button data-ng-click=\"toggle('assignment')\" class=\"btn btn-default\" >Cancel</button>\n" +
		"                </p>\n" +
		"            </div>\n" +
		"        </span>\n" +
		"        \n" +
		"    </tab>\n" +
		"    <tab >\n" +
		"        <tab-heading>\n" +
		"            <h3 >Relationships ({{ ticket.associated.length }}) <img src=\"/images/spinner.gif\" data-ng-show=\"spinners.relationship > 0\" /></h3>\n" +
		"        </tab-heading>\n" +
		"        \n" +
		"        <p data-ng-show=\"ticket.associated.length == 0\" >There are no relationships</p>\n" +
		"        <table class=\"app-data striped ticket-subrecord\" data-ng-show=\"ticket.associated.length > 0\" >\n" +
		"            <thead >\n" +
		"                <tr >\n" +
		"                    <th >Type</th>\n" +
		"                    <th >Ticket</th>\n" +
		"                    <th >Pinned</th>\n" +
		"                    <th colspan=\"2\" >Options</th>\n" +
		"                    <th >Delete</th>\n" +
		"                </tr>\n" +
		"            </thead>\n" +
		"            <tbody >\n" +
		"                <tr data-ng-repeat=\"association in ticket.associated\" >\n" +
		"                    <td >{{ relationshipTypeToDesc(association.relationshipType) }}</td>\n" +
		"                    <td >\n" +
		"                        <a ui-sref=\"ticketview({id: association.trgTicketId})\" >\n" +
		"                            {{ association.trgSummary }}\n" +
		"                        </a>\n" +
		"                    </td>\n" +
		"                    <td ><input type=\"checkbox\" data-ng-model=\"association.pinned\" data-ng-click=\"togglePinned(association)\" /></td>\n" +
		"                    <td ><span class=\"assoc-up-down-container\"><button data-ng-show=\"$index < (ticket.associated.length - 1)\" data-ng-click=\"moveAssociated(association.id, 'down')\" title=\"Move this association down\" class=\"btn btn-warning\" ><span class=\"glyphicon glyphicon-chevron-down\" ></span></button></span></td>\n" +
		"                    <td ><span class=\"assoc-up-down-container\"><button data-ng-show=\"$index > 0\" data-ng-click=\"moveAssociated(association.id, 'up')\" title=\"Move this association up\" class=\"btn btn-warning\" ><span class=\"glyphicon glyphicon-chevron-up\" ></span></button></span></td>\n" +
		"                    <td ><span class=\"assoc-delete-container\"><button data-ng-click=\"deleteAssociated(association.id)\" title=\"Delete this association\" class=\"btn btn-danger\" ><span class=\"glyphicon glyphicon-remove\" ></span></button></span></td>\n" +
		"                </tr>\n" +
		"            </tbody>\n" +
		"        </table>\n" +
		"\n" +
		"        <span ng-switch on=\"adding.relationship\" >\n" +
		"            <p data-ng-switch-when=\"false\">\n" +
		"                <button data-ng-click=\"toggle('relationship')\" class=\"btn btn-primary\" >Add Relationship</button>\n" +
		"            </p>\n" +
		"            <div data-ng-switch-when=\"true\">\n" +
		"                <p >\n" +
		"                    <select data-ng-model=\"newRelationship.relType\" data-ng-options=\"oneRelationship.description for oneRelationship in relationshipTypes\">\n" +
		"                        <option value=\"\" >Please select a type</option>\n" +
		"                    </select>\n" +
		"                    <select data-ng-model=\"newRelationship.trgTicket\" data-ng-options=\"oneTicket.summary for oneTicket in allTickets | excludeItself:ticket_id\">\n" +
		"                        <option value=\"\" >Please select a ticket</option>\n" +
		"                    </select>\n" +
		"                </p>\n" +
		"                <p >\n" +
		"                    <button data-ng-click=\"saveRelationship()\" class=\"btn btn-primary\" >Save</button>\n" +
		"                    <button data-ng-click=\"toggle('relationship')\" class=\"btn btn-default\" >Cancel</button>\n" +
		"                </p>\n" +
		"            </div>\n" +
		"        </span>\n" +
		"        \n" +
		"    </tab>\n" +
		"    <tab >\n" +
		"        <tab-heading>\n" +
		"            <h3 >Time Spent (<span title=\"This ticket\" >{{ ticket.timeSpentTotals.total_spent }}</span> | <span title=\"This ticket and its children\" >{{ ticket.timeSpentTotals.total_spent_inc_children }}</span>) <img src=\"/images/spinner.gif\" data-ng-show=\"spinners.timeSpent > 0\" /></h3>\n" +
		"        </tab-heading>\n" +
		"        \n" +
		"        <table class=\"app-data striped ticket-subrecord\" >\n" +
		"            <thead >\n" +
		"                <tr >\n" +
		"                    <th >Start Time</th>\n" +
		"                    <th >Duration</th>\n" +
		"                    <th >User</th>\n" +
		"                    <th >Notes</th>\n" +
		"                </tr>\n" +
		"            </thead>\n" +
		"            <tbody >\n" +
		"                <tr data-ng-show=\"ticket.timesSpent.length == 0\" >\n" +
		"                    <td colspan=\"4\" >No time has been recorded against this ticket</td>\n" +
		"                </tr>\n" +
		"                <tr data-ng-repeat=\"timeSpent in ticket.timesSpent\" >\n" +
		"                    <td >{{ timeSpent.startDatetime | date:'dd/MM/yyyy&nbsp;H:mm:ss' }}</td>\n" +
		"                    <td >{{ timeSpent.durationFormatted }}</td>\n" +
		"                    <td >{{ timeSpent.userName }}</td>\n" +
		"                    <td >{{ timeSpent.notes }}</td>\n" +
		"                </tr>\n" +
		"                <tr >\n" +
		"                    <td ><br /></td>\n" +
		"                    <th >This Ticket</th>\n" +
		"                    <th colspan=\"2\">This Ticket and Children</th>\n" +
		"                </tr>\n" +
		"                <tr >\n" +
		"                    <th >Total</th>\n" +
		"                    <td >{{ ticket.timeSpentTotals.total_spent }}</td>\n" +
		"                    <td colspan=\"2\">{{ ticket.timeSpentTotals.total_spent_inc_children }}</td>\n" +
		"                </tr>\n" +
		"                <tr >\n" +
		"                    <th >Remaining</th>\n" +
		"                    <td >{{ ticket.timeSpentTotals.total_remaining }}</td>\n" +
		"                    <td colspan=\"2\">{{ ticket.timeSpentTotals.total_remaining_inc_children }}</td>\n" +
		"                </tr>\n" +
		"            </tbody>\n" +
		"        </table>\n" +
		"\n" +
		"        <span ng-switch on=\"adding.timeSpent\" >\n" +
		"            <p data-ng-switch-when=\"false\">\n" +
		"                <button data-ng-click=\"toggle('timeSpent')\" class=\"btn btn-primary\" >Record time spent</button>\n" +
		"            </p>\n" +
		"            <div data-ng-switch-when=\"true\">\n" +
		"                <table >\n" +
		"                    <tbody >\n" +
		"                        <tr >\n" +
		"                            <th >Start Time</th>\n" +
		"                            <td ><input type=\"text\" data-ng-model=\"newTimeSpent.startDatetime\" /></td>\n" +
		"                        </tr>\n" +
		"                        <tr >\n" +
		"                            <th >Duration</th>\n" +
		"                            <td ><input type=\"text\" data-ng-model=\"newTimeSpent.duration\" /></td>\n" +
		"                        </tr>\n" +
		"                        <tr >\n" +
		"                            <th >Notes</th>\n" +
		"                            <td ><textarea data-ng-model=\"newTimeSpent.notes\" ></textarea></td>\n" +
		"                        </tr>\n" +
		"                    </tbody>\n" +
		"                </table>\n" +
		"                <p >\n" +
		"                    <button data-ng-click=\"saveTimeSpent()\" class=\"btn btn-primary\" >Save</button>\n" +
		"                    <button data-ng-click=\"toggle('timeSpent')\" class=\"btn btn-default\" >Cancel</button>\n" +
		"                </p>\n" +
		"            </div>\n" +
		"        </span>\n" +
		"        \n" +
		"    </tab>\n" +
		"</tabset>\n" +
		"\n" +
		"\n" +
		"<h3 >Comments <img src=\"/images/spinner.gif\" data-ng-show=\"spinners.comment > 0\" /></h3>\n" +
		"<p data-ng-show=\"ticket.comments.length == 0\" >There are no comments</p>\n" +
		"<table data-ng-show=\"ticket.comments.length > 0\" >\n" +
		"    <tbody >\n" +
		"        <tr data-ng-repeat=\"comment in ticket.comments\" >\n" +
		"            <td >\n" +
		"                {{ comment.createdAt | date:'dd/MM/yyyy H:mm:ss' }}&nbsp;{{ comment.addedByUserName }}\n" +
		"                <div class=\"formatted-block comment\" data-ng-bind-html=\"comment.detailsMarkdown | sanitise\" ></div>\n" +
		"            </td>\n" +
		"        </tr>\n" +
		"    </tbody>\n" +
		"</table>\n" +
		"\n" +
		"<span ng-switch on=\"adding.comment\" >\n" +
		"    <p data-ng-switch-when=\"false\">\n" +
		"        <button data-ng-click=\"toggle('comment')\" class=\"btn btn-primary\" >Add Comment</button>\n" +
		"    </p>\n" +
		"    <div data-ng-switch-when=\"true\">\n" +
		"        <p >\n" +
		"            Your comment...<br />\n" +
		"            <textarea data-ng-model=\"newComment.detail\" cols=\"80\" rows=\"8\" ></textarea>\n" +
		"        </p>\n" +
		"        <p >\n" +
		"            <button data-ng-click=\"saveComment()\" class=\"btn btn-primary\" >Save</button>\n" +
		"            <button data-ng-click=\"toggle('comment')\" class=\"btn btn-default\" >Cancel</button>\n" +
		"        </p>\n" +
		"    </div>\n" +
		"</span>\n" +
		"\n" +
		"\n" +
		"\n" +
		"</span>\n" +
		"");
}]);

angular.module("partials/tickets.html", []).run(["$templateCache", function($templateCache) {
	"use strict";
	$templateCache.put("partials/tickets.html",
		"<h2 >Tickets</h2>\n" +
		"\n" +
		"<p >Add a <a ui-sref=\"ticketadd()\" >new ticket</a>.</p>\n" +
		"\n" +
		"<div class=\"panel panel-info\">\n" +
		"    <div class=\"panel-heading\" data-ng-click=\"toggleShowFilter()\" >\n" +
		"        <h3 class=\"panel-title\">Filter tickets\n" +
		"            <span aria-hidden=\"true\" class=\"glyphicon\" data-ng-class=\"filterIsVisible ? 'glyphicon-triangle-bottom' : 'glyphicon-triangle-right'\"></span>\n" +
		"        </h3>\n" +
		"    </div>\n" +
		"    <div class=\"panel-body\" data-ng-show=\"filterIsVisible\" >\n" +
		"        <form data-ng-submit=\"applyFilter()\" >\n" +
		"            <table class=\"record-filter\" >\n" +
		"                <tr >\n" +
		"                    <th >Summary</th>\n" +
		"                    <td ><input type=\"text\" data-ng-model=\"filter.summary\" size=\"60\" /></td>\n" +
		"                </tr>\n" +
		"                <tr >\n" +
		"                    <th >Description</th>\n" +
		"                    <td ><input type=\"text\" data-ng-model=\"filter.description\" size=\"60\" /></td>\n" +
		"                </tr>\n" +
		"                <tr >\n" +
		"                    <th >Project</th>\n" +
		"                    <td >\n" +
		"                        <select data-ng-model=\"filter.projectId\" data-ng-options=\"project.id as project.name for project in projectData\">\n" +
		"                            <option value=\"\" >(All projects)</option>\n" +
		"                        </select>\n" +
		"                    </td>\n" +
		"                </tr>\n" +
		"                <tr >\n" +
		"                    <th >Ticket Types</th>\n" +
		"                    <td >\n" +
		"                        <table>\n" +
		"                            <tr data-ng-repeat=\"ticketType in staticData.types\" >\n" +
		"                                <td ><label for=\"ticket-type-{{ticketType.code}}\" ><input type=\"checkbox\" data-ng-model=\"filter.type[ticketType.code]\" id=\"ticket-type-{{ticketType.code}}\" />{{ ticketType.desc }}</label></td>\n" +
		"                            </tr>\n" +
		"                        </table>\n" +
		"                    </td>\n" +
		"                </tr>\n" +
		"                <tr >\n" +
		"                    <th >Created By</th>\n" +
		"                    <td >\n" +
		"                        <select data-ng-model=\"filter.createdByUserId\" data-ng-options=\"user.id as user.userName for user in userData\">\n" +
		"                            <option value=\"\" >(Any user)</option>\n" +
		"                        </select>\n" +
		"                    </td>\n" +
		"                </tr>\n" +
		"                <tr >\n" +
		"                    <th >Priorities</th>\n" +
		"                    <td >\n" +
		"                        <table>\n" +
		"                            <tr data-ng-repeat=\"priorityType in staticData.priorities\" >\n" +
		"                                <td ><label for=\"ticket-priority-{{priorityType.code}}\" ><input type=\"checkbox\" data-ng-model=\"filter.priority[priorityType.code]\" id=\"ticket-priority-{{priorityType.code}}\" />{{ priorityType.desc }}</label></td>\n" +
		"                            </tr>\n" +
		"                        </table>\n" +
		"                    </td>\n" +
		"                </tr>\n" +
		"                <tr >\n" +
		"                    <th >Statuses</th>\n" +
		"                    <td >\n" +
		"                        <table>\n" +
		"                            <tr data-ng-repeat=\"statusType in staticData.statuses\" >\n" +
		"                                <td ><label for=\"ticket-status-{{statusType.code}}\" ><input type=\"checkbox\" data-ng-model=\"filter.status[statusType.code]\" id=\"ticket-status-{{statusType.code}}\" />{{ statusType.desc }}</label></td>\n" +
		"                            </tr>\n" +
		"                        </table>\n" +
		"                    </td>\n" +
		"                </tr>\n" +
		"                <tr >\n" +
		"                    <th >Assignments</th>\n" +
		"                    <td >\n" +
		"                        <select data-ng-model=\"filter.assignment.userId\" data-ng-options=\"user.id as user.userName for user in userData\">\n" +
		"                            <option value=\"\" >(Any user)</option>\n" +
		"                        </select>\n" +
		"                        as\n" +
		"                        <select data-ng-model=\"filter.assignment.level\" >\n" +
		"                            <option value=\"\" >Any</option>\n" +
		"                            <option value=\"primary\" >Primary</option>\n" +
		"                            <option value=\"other\" >Other</option>\n" +
		"                        </select>\n" +
		"                    </td>\n" +
		"                </tr>\n" +
		"            </table>\n" +
		"            <input type=\"submit\" class=\"btn btn-primary\" value=\"Apply\" />\n" +
		"            <button data-ng-click=\"clearFilter($event)\" >Clear</button>\n" +
		"        </form>\n" +
		"    </div>\n" +
		"</div>\n" +
		"\n" +
		"<p data-ng-show=\"isLoading()\" ><img src=\"images/spinner.gif\" /> Loading...</p>\n" +
		"<span data-ng-hide=\"isLoading()\" >\n" +
		"    <p data-ng-show=\"tickets.length == 0\">Sorry, no tickets match your criteria</p>\n" +
		"\n" +
		"    <h3 data-ng-show=\"tickets.length > 0\" >{{ tickets.length }} tickets...</h3>\n" +
		"    <table class=\"app-data striped\" data-ng-show=\"tickets.length > 0\">\n" +
		"        <thead>\n" +
		"            <th >Id</th>\n" +
		"            <th >Project</th>\n" +
		"            <th >Type</th>\n" +
		"            <th >Summary</th>\n" +
		"            <th >Primary</th>\n" +
		"            <th >Priority</th>\n" +
		"            <th >Status</th>\n" +
		"            <th >Options</th>\n" +
		"        </thead>\n" +
		"        <tbody>\n" +
		"            <tr data-ng-repeat=\"ticket in tickets\" >\n" +
		"                <td >{{ ticket.id }}</td>\n" +
		"                <td >{{ ticket.project.name }}</td>\n" +
		"                <td >{{ ticket.getTicketTypeDesc() }}</td>\n" +
		"                <td >{{ ticket.summary }}</td>\n" +
		"                <td >{{ ticket.getPrimaryAssigneeName() }}</td>\n" +
		"                <td >{{ ticket.getPriorityDesc() }}</td>\n" +
		"                <td >{{ ticket.getStatusDesc() }}</td>\n" +
		"                <td >\n" +
		"                    <a ui-sref=\"ticketview({id: ticket.id})\" >&nbsp;Details/Edit</a>\n" +
		"                </td>\n" +
		"            </tr>\n" +
		"        </tbody>\n" +
		"    </table>\n" +
		"</span>\n" +
		"");
}]);

angular.module("partials/version-history.html", []).run(["$templateCache", function($templateCache) {
	"use strict";
	$templateCache.put("partials/version-history.html",
		"<h2 >Version History</h2>\n" +
		"\n" +
		"<table class=\"version-history\" >\n" +
		"    <thead>\n" +
		"        <tr ><th >Version</th><th >Release Date</th><th >Detail</th></tr>\n" +
		"    </thead>\n" +
		"    <tbody>\n" +
		"        <tr class=\"revision\" >\n" +
		"            <td >0.3.6</td>\n" +
		"            <td>04/08/2015</td>\n" +
		"            <td >The \"list\" screens now show a \"Loading...\" message while the records are retrieved from the server.</td>\n" +
		"        </tr>\n" +
		"        <tr class=\"revision\" >\n" +
		"            <td >0.3.5</td>\n" +
		"            <td>04/08/2015</td>\n" +
		"            <td >Ticket view now shows total time spent, even if no time recorded against that ticket.</td>\n" +
		"        </tr>\n" +
		"        <tr class=\"revision\" >\n" +
		"            <td >0.3.4</td>\n" +
		"            <td>01/08/2015</td>\n" +
		"            <td >Put ticket assignments, relationships and time spent into tabs. Looks lovely.</td>\n" +
		"        </tr>\n" +
		"        <tr class=\"revision\" >\n" +
		"            <td >0.3.3</td>\n" +
		"            <td>01/08/2015</td>\n" +
		"            <td >Improved the time spent code where it updates the totals after a save.</td>\n" +
		"        </tr>\n" +
		"        <tr class=\"revision\" >\n" +
		"            <td >0.3.2</td>\n" +
		"            <td>01/08/2015</td>\n" +
		"            <td >Added a delete option to relationships.</td>\n" +
		"        </tr>\n" +
		"        <tr class=\"revision\" >\n" +
		"            <td >0.3.1</td>\n" +
		"            <td>01/08/2015</td>\n" +
		"            <td >Stopped relationship form showing the current ticket in the related ticket list.</td>\n" +
		"        </tr>\n" +
		"        <tr class=\"minor\" >\n" +
		"            <td >0.3.0</td>\n" +
		"            <td>26/07/2015</td>\n" +
		"            <td >\n" +
		"                Fixed bug with logging in (oops).<br />\n" +
		"                Added the ability to record time against a ticket; show total time recorded and time remaining.\n" +
		"            </td>\n" +
		"        </tr>\n" +
		"        <tr class=\"minor\" >\n" +
		"            <td >0.2.0</td>\n" +
		"            <td>24/07/2015</td>\n" +
		"            <td >Now uses bower to manage all the 3rd-party JS resources, including AngularJS itself</td>\n" +
		"        </tr>\n" +
		"        <tr class=\"revision\" >\n" +
		"            <td >0.1.1</td>\n" +
		"            <td>24/07/2015</td>\n" +
		"            <td >Added version history</td>\n" +
		"        </tr>\n" +
		"        <tr class=\"minor\" >\n" +
		"            <td >0.1.0</td>\n" +
		"            <td>23/07/2015</td>\n" +
		"            <td >Initial version</td>\n" +
		"        </tr>\n" +
		"    </tbody>\n" +
		"</table>\n" +
		"    ");
}]);
