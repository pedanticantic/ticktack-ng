'use strict';

// Declare app level module which depends on views, and components
var ticktack = angular.module('ticktack', [
    'ui.router',
    'ui.bootstrap',
    'ngResource',
    'templates-main'
]);

/* This is to do with intercepting a 401 coming from the server - it means the
   user was logged in, but isn't now.
ticktack.config(function ($httpProvider) {

    $httpProvider.interceptors.push(function ($timeout, $q, $injector) {
        var loginModal, $http, $state;

        // this trick must be done so that we don't receive
        // `Uncaught Error: [$injector:cdep] Circular dependency found`
        $timeout(function () {
            loginModal = $injector.get('loginModal');
            $http = $injector.get('$http');
            $state = $injector.get('$state');
        });

        return {
            responseError: function (rejection) {
                if (rejection.status !== 401) {
                    return rejection;
                }

                var deferred = $q.defer();

                loginModal()
                    .then(function () {
                        deferred.resolve( $http(rejection.config) );
                    })
                    .catch(function () {
                        $state.go('welcome');
                        deferred.reject(rejection);
                    });

                return deferred.promise;
            }
        };
    });

});
*/

// For API calls, we need to convert the app host to the server host and add
// the auth headers.
ticktack.config(function ($httpProvider) {
    $httpProvider.interceptors.push(function ($q, $location, $rootScope) {
        return {
            'request': function (config) {
                if (config.url.substr(0, 4) == 'api/') {
                    var serverHost = $location.$$host;
                    switch (serverHost) {
                        case 'ticktack.ng.dev':
                            serverHost = 'http://ticktack.dev:8088/';
                            break;
                        case 'ticktack-ng.pedanticantic.click':
                            serverHost = 'http://ticktack.pedanticantic.click/';
                            break;
                    }
                    config.url = serverHost + config.url;
                    // Sort out the user credentials. Only if it's a call to the API
                    // and it's not a login request, and we have a valid user.
                    if ($rootScope.currentUser && config.url.indexOf('/login') == -1) {
                        config.headers.Authorization = $rootScope.currentUser.getAuthorization();
                    }
                }
                return config || $q.when(config);
            }
        }
    });
});

ticktack.filter('sanitise', ['$sce', function($sce) {
    return function(htmlCode){
        return $sce.trustAsHtml(htmlCode);
    }
}]);

ticktack.filter('excludeItself', function() {
    return function(inputArray, ticketId) {
        return inputArray.filter(function(item) {
            // If the array element's ticket id matches the one passed in, exclude it from the result.
            return !ticketId || !angular.equals(item.id, ticketId);
        });
    };
});

ticktack.run(function ($rootScope, $state, $injector, $http, loginModal, UsersApi, referenceData) {

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
        var userSession = $injector.get('LoggedInUser');
        var userCredentials = userSession.getUser();
        if (toState.data.forgetLogin === true) {
            $rootScope.currentUser = undefined;
            userSession.setUser(undefined, undefined);
        } else {
            var requireLogin = toState.data.requireLogin;

            if (requireLogin && typeof $rootScope.currentUser === 'undefined') {
                event.preventDefault();
                
                // User isn't logged in. See if we still have their credentials. If so
                // auto-log them in. This happens if user refreshes the screen.
                if (userCredentials &&
                    userCredentials.username != '' &&
                    userCredentials.username != 'undefined' &&
                    userCredentials.username != undefined &&
                    userCredentials.password != '' &&
                    userCredentials.password != 'undefined' &&
                    userCredentials.password != undefined) {
                    UsersApi.login(userCredentials.username, userCredentials.password)
                        .then(function (user) {
                            $rootScope.currentUser = user;
                            $state.go(toState.name, toParams)
                        }, function() {
                            console.log('Something went wrong with auto login', userCredentials);
                        });
                } else {
                    
                    // User is not logged in and must have explicitly logged out. Show the login form.
                    loginModal()
                        .then(function () {
                            return $state.go(toState.name, toParams);
                        })
                        .catch(function () {
                            // User canceled the login modal. Go to the default page.
                            return $state.go('home');
                        });
                }
            }
        }
    });
    
    // Call the API endpoint to get all the static reference data.
    $http({
        method: "get",
        url: "api/v1/referenceData",
    }).then(
        function success(response) {
            if (response.data.success) {
                referenceData.setReferenceData(response.data.referenceData);
            }
        },
        function failure(response) {
            alert('Failed to load the reference data.');
            $location.path('/home');
        }
    );

});


ticktack.factory('assignmentModel', [function assignmentFactory() {
    
    var Assignment = function Assignment() {
        // Attributes.
        this.id = null;
        this.ticketId = null;
        this.userId = null;
        this.userName = null;
        this.sortOrder = null;
    };
    
    Assignment.prototype = {
        
        getFieldMap: function getFieldMap() {
            return {
                id: 'id',
                ticketId: 'ticket_id',
                userId: 'user_id',
                userName: 'user_name',
                sortOrder: 'sort_order',
            }
        },
    
        getAssigneeType: function getAssigneeType() {
            return (this.sortOrder == 1 ? 'Primary' : 'Other');
        },
        
        // @TODO: I'm sure this can be made generic, rather than specific to each model.
        // See the equivalent method in Ticket.js
        matches: function matches(filter) {
            var assignmentMatches = true;
            for( var filterType in filter) {
                var doesMatch = false;
                if (this.hasOwnProperty(filterType)) {
                    var filterCriteria = filter[filterType];
                    if (Array.isArray(filterCriteria)) {
                        for(var criteriaIndex = 0 ; criteriaIndex < filterCriteria.length ; criteriaIndex++) {
                            doesMatch = doesMatch || this[filterType] == filterCriteria[criteriaIndex];
                        }
                    } else {
                        // We have a special case here - if the filter starts with "!", we negate the
                        // match result.
                        if (typeof filterCriteria == 'string' && filterCriteria.substr(0, 1) == '!') {
                            filterCriteria = filterCriteria.substr(1);
                            doesMatch = this[filterType] != filterCriteria;
                        } else {
                            doesMatch = this[filterType] == filterCriteria;
                        }
                    }
                }
                assignmentMatches = assignmentMatches && doesMatch;
            }
            return assignmentMatches;
        }
        
    };
    
    return Assignment;
}]);


ticktack.factory('commentModel', [function ticketFactory() {
    
    var Comment = function Comment() {
        // Attributes.
        this.id = null;
        this.ticketId = null;
        this.addedByUserId = null;
        this.details = null;
        this.detailsMarkdown = null;
        this.createdAt = null;
        this.addedByUserName = null;
    };
    
    Comment.prototype = {
        
        getFieldMap: function getFieldMap() {
            return {
                id: 'id',
                ticketId: 'ticket_id',
                addedByUserId: 'added_by_user_id',
                details: 'details',
                detailsMarkdown: 'details_markdown',
                addedByUserName: 'added_by_user_name',
                createdAt: 'created_at'
            }
        }
        
    };
    
    return Comment;
}]);


ticktack.factory('projectModel', [function projectFactory() {
    
    var Project = function Project() {
        // Attributes.
        this.id = null;
        this.name = null;
        this.description = null;
        this.descriptionAsMarkdown = null;
        this.leadUserId = null;
        this.leadUserName = null;
    };
    
    Project.prototype = {
        
        getFieldMap: function getFieldMap() {
            return {
                id: 'id',
                name: 'name',
                description: 'description',
                descriptionAsMarkdown: 'description_markdown',
                leadUserId: 'lead_user_id',
                leadUserName: 'lead_user_username'
            }
        }
        
    };
    
    return Project;
}]);


ticktack.factory('relationshipModel', [function relationshipFactory() {
    
    var Relationship = function Relationship() {
        // Attributes.
        this.id = null;
        this.relationshipType = null;
        this.srcTicketId = null;
        this.trgSummary = null;
        this.srcSortOrder = null;
        this.trgTicketId = null;
        this.trgSortOrder = null;
        this.pinned = null;

    };
    
    Relationship.prototype = {
        
        getFieldMap: function getFieldMap() {
            return {
                id: 'id',
                relationshipType: 'relationship_type',
                srcTicketId: 'src_ticket_id',
                trgSummary: 'trg_summary',
                srcSortOrder: 'src_sort_order',
                trgTicketId: 'trg_ticket_id',
                trgSortOrder: 'trg_sort_order',
                pinned: 'pinned'
            }
        }
        
    };
    
    return Relationship;
}]);


ticktack.factory('ticketModel', ['referenceData', 'projectModel', 'assignmentModel', 'relationshipModel', 'commentModel', 'timeSpentModel', function ticketFactory(referenceData, projectModel, assignmentModel, relationshipModel, commentModel, timeSpentModel) {
    
    var Ticket = function Ticket() {
        // Attributes.
        this.id = null;
        this.projectId = -1;
        this.ticketType = 'new feature';
        this.summary = null;
        this.description = null;
        this.descriptionMarkdown = null;
        this.createdByUserId = null;
        this.createdByUserName = null;
        this.priority = 'medium';
        this.status = 'new';
        this.dueDatetime = null;
        this.dueDatetimeForView = null;
        this.estimatedLengthSeconds = null;
        this.estimatedLengthForView = null;
        
        this.project = {};
        this.assignments = [];
        this.associated = [];
        this.comments = [];
        this.timesSpent = [];
        this.timeSpentTotals = {};
        
        this.refData = referenceData;
    };
    
    Ticket.prototype = {
        
        getFieldMap: function getFieldMap() {
            return {
                id: 'id',
                projectId: 'project_id',
                ticketType: 'ticket_type',
                summary: 'summary',
                description: 'description',
                descriptionMarkdown: 'description_markdown',
                createdByUserId: 'created_by_user_id',
                createdByUserName: 'created_by_name',
                priority: 'priority',
                status: 'status',
                dueDatetime: 'due_datetime',
                estimatedLengthSeconds: 'estimated_length_seconds',
                estimatedLengthForView: 'estimated_length_for_view',
                project: function(projectData) {
                    // We expect one project.
                    var objectTranslator = new ticktack.ObjectTranslator();
                    return objectTranslator.translate(projectData, new projectModel());
                },
                assignments: function(assignmentData) {
                    // We expect a list of assignments.
                    var objectTranslator = new ticktack.ObjectTranslator();
                    return objectTranslator.translateArray(assignmentData, function() { return new assignmentModel(); });
                },
                associated: function(relationshipData) {
                    // We expect a list of relationships.
                    var objectTranslator = new ticktack.ObjectTranslator();
                    return objectTranslator.translateArray(relationshipData, function() { return new relationshipModel(); });
                },
                comments: function(commentData) {
                    // We expect a list of assignments.
                    var objectTranslator = new ticktack.ObjectTranslator();
                    return objectTranslator.translateArray(commentData, function() { return new commentModel(); });
                },
                timesSpent: function(timesSpentData) {
                    // We expect a list of time spent records.
                    var objectTranslator = new ticktack.ObjectTranslator();
                    return objectTranslator.translateArray(timesSpentData, function() { return new timeSpentModel(); });
                },
                timeSpentTotals: function(timeSpentTotalsData) {
                    return timeSpentTotalsData;
                }
            }
        },
        
        triggerPostTranslate: function triggerPostTranslate() {
            if (this.dueDatetime == '') {
                this.dueDatetimeForView = '';
            } else {
                // This is a hack to make it ignore time zones
                this.dueDatetime = this.dueDatetime.substr(0, 19);
                
                var tmp = new Date(this.dueDatetime);
                this.dueDatetimeForView = tmp.toLocaleDateString() + ' ' + tmp.toLocaleTimeString();
                this.dueDatetimeForView = this.dueDatetimeForView.substr(0, this.dueDatetimeForView.length - 3); // Remove the seconds component of the time.
            }
        },
        
        setCreatedByUserId: function setCreatedByUserId(newUserId) {
            this.createdByUserId = newUserId;
        },
        
        getCreatedByUserId: function getCreatedByUserId() {
            return this.createdByUserId;
        },
    
        getPrimaryAssigneeName: function getPrimaryAssigneeName() {
            if (this.assignments.length == 0) {
                return '[none]';
            }
            return this.assignments[0].userName;
        },
        
        // Method to return true if the ticket matches the given criteria.
        // The filter is an object; each property is of the form:
        //   <ticketProperty>: <filterValue>
        // filterValue can be a scalar, or it can be an array.
        // If it's an array, the filter matches if any elements in the array match the ticket property value.
        // Method returns true if every property of the filter is a positive match.
        // @TODO: I'm sure this can be made generic, rather than specific to each model.
        matches: function matches(filter) {
            var ticketMatches = true;
            for( var filterType in filter) {
                var doesMatch = false;
                if (this.hasOwnProperty(filterType)) {
                    var filterCriteria = filter[filterType];
                    // Assignments is a special case. We need to loop through them and see if any match
                    // the criteria.
                    if (filterType == 'assignments') {
                        for(var assIndex = 0 ; assIndex < this.assignments.length ; assIndex++) {
                            doesMatch = doesMatch || this.assignments[assIndex].matches(filterCriteria);
                        }
                    } else {
                        if (Array.isArray(filterCriteria)) {
                            for(var criteriaIndex = 0 ; criteriaIndex < filterCriteria.length ; criteriaIndex++) {
                                doesMatch = doesMatch || this[filterType] == filterCriteria[criteriaIndex];
                            }
                        } else {
                            doesMatch = this[filterType] == filterCriteria;
                        }
                    }
                }
                ticketMatches = ticketMatches && doesMatch;
            }
            return ticketMatches;
        },
        
        // Methods to return the description for the given field and value.
        getTicketTypeDesc: function getTicketTypeDesc() {
            return this.refData.getDesc('ticket_types', this.ticketType);
        },
        getPriorityDesc: function getPriorityDesc() {
            return this.refData.getDesc('ticket_priorities', this.priority);
        },
        getStatusDesc: function getStatusDesc() {
            return this.refData.getDesc('ticket_statuses', this.status);
        }
        
    };
    
    return Ticket;
}]);


ticktack.factory('timeSpentModel', [function ticketFactory() {
    
    var TimeSpent = function TimeSpent() {
        // Attributes.
        this.id = null;
        this.ticketId = null;
        this.userId = null;
        this.userName = null;
        this.startDatetime = null;
        this.duration = null;
        this.durationFormatted = null;
        this.notes = null;
    };
    
    TimeSpent.prototype = {
        
        getFieldMap: function getFieldMap() {
            return {
                id: 'id',
                ticketId: 'ticket_id',
                userId: 'user_id',
                userName: 'username',
                startDatetime: 'start_datetime',
                duration: 'duration',
                durationFormatted: 'duration_formatted',
                notes: 'notes'
            }
        }
        
    };
    
    return TimeSpent;
}]);


ticktack.factory('userModel', [function userFactory() {
    
    var User = function User() {
        // Attributes.
        this.id = null;
        this.userName = null;
    };
    
    User.prototype = {
        
        getFieldMap: function getFieldMap() {
            return {
                id: 'id',
                userName: 'username'
            }
        }
        
    };
    
    return User;
}]);


ticktack.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');

    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'partials/home.html',
            // ...
            data: {
                requireLogin: false
            }
        })
        
        .state('register', {
            url: '/register',
            templateUrl: 'partials/register.html',
            controller: 'RegisterCtrl',
            // ...
            data: {
                requireLogin: false
            }
        })
        
        .state('version-history', {
            url: '/version-history',
            templateUrl: 'partials/version-history.html',
            // ...
            data: {
                requireLogin: false
            }
        })
        
        // Projects
        .state('projects', {
            url: '/projects',
            templateUrl: 'partials/projects.html',
            controller: 'ProjectsCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        .state('projectcreate', {
            url: '/projects/create',
            templateUrl: 'partials/project-edit.html',
            controller: 'ProjectEditCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        .state('projectview', {
            url: '/projects/:id',
            templateUrl: 'partials/project-view.html',
            controller: 'ProjectViewCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        .state('projectedit', {
            url: '/projects/:id/edit',
            templateUrl: 'partials/project-edit.html',
            controller: 'ProjectEditCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        
        // Tickets
        // They have a _lot_ of filters!
        .state('tickets', {
            url: '/tickets?summary&description&projectId&ticketTypes&createdByUserId&statuses&priorities&assignmentUserIds&assignmentLevel',
            templateUrl: 'partials/tickets.html',
            controller: 'TicketsCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        .state('ticketview', {
            url: '/tickets/:id',
            templateUrl: 'partials/ticket-view.html',
            controller: 'TicketViewCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        .state('ticketadd', {
            url: '/ticket',
            templateUrl: 'partials/ticket-view.html',
            controller: 'TicketViewCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        
        // Logout
        .state('logout', {
            url: '/logout',
            templateUrl: 'partials/logout.html',
            // ...
            data: {
                forgetLogin: true
            }
        })
        .state('app', {
            abstract: true,
            // ...
            data: {
                requireLogin: true // this property will apply to all children of 'app'
            }
        })

});

ticktack.service('LoggedInUser', function () {
    
    var retrieveUser = function retrieveUser() {
        if (localStorage.username && localStorage.password) {
            user.username = localStorage.username;
            user.password = localStorage.password;
        }
    }
    
    var saveUser = function saveUser() {
        localStorage.username = user.username;
        localStorage.password = user.password;
    }
    
    /**
     * @var Underlying object holding the user details.
     */
    var user = {
        username: '',
        password: ''
    }
    retrieveUser();
    
    /**
     * Method to set the username and password in the internal variable.
     * @param username
     * @param password
     */
    function setUser(username, password) {
        user.username = username;
        user.password = password;
        saveUser();
    }
    
    /**
     * Method to return the internal variable.
     * @return user
     */
    function getUser() {
        var un = user.username;
        var pw = user.password;
        return {username: un, password: pw};
    }
    
    /**
     * Method to return the id from the given list of users, where the user name
     * matches the currently logged in user.
     */
    function getLoggedInUserId(userData) {
        if (userData == null) {
            return null;
        }
        for(var userIdx = 0 ; userIdx < userData.length ; userIdx++) {
            if (userData[userIdx].userName == user.username) {
                return userData[userIdx].id;
            }
        }
        return null;
    }
    
    /**
     * Method to return the authorization string for this user.
     * @return string
     */
    function getAuthorization() {
        return 'Basic ' + window.btoa(user.username + ':' + user.password);
    }

    return {
        setUser: setUser,
        getUser: getUser,
        getAuthorization: getAuthorization,
        getLoggedInUserId: getLoggedInUserId
    };

});

ticktack.ObjectTranslator = (function () {

    var ObjectTranslator = function ObjectTranslator() {
    };
    
    /**
     * Method to take an array of data rows, and translate each one into an object.
     * @param array data The array of rows of records.
     * @param object modelInstance An (empty) instance of the model.
     *
     * @return object[] An array of models.
     */
    function translate(data, modelInstance) {
        // Check that the model has a field map method.
        if (modelInstance.getFieldMap && typeof modelInstance.getFieldMap == 'function') {
            var fieldMap = modelInstance.getFieldMap();
            // Loop through the fields, assigning the object property to the dataset value.
            for (var modelField in fieldMap) {
                if (modelInstance.hasOwnProperty(modelField)) {
                    if (typeof fieldMap[modelField] == 'function') {
                        modelInstance[modelField] = fieldMap[modelField](data[modelField]);
                    } else {
                        modelInstance[modelField] = data[fieldMap[modelField]];
                    }
                }
            }
            // See if the model has a "post-translate" trigger. If so, run it.
            if (modelInstance.triggerPostTranslate && typeof modelInstance.triggerPostTranslate == 'function') {
                modelInstance.triggerPostTranslate();
            }
        }
        // Return the array of instances of the model.
        return modelInstance;
    }
    
    /**
     * Method to take an array of data rows, and translate each one into an object.
     * @param array dataSet The array of rows of records.
     * @param function modelInstanceCallback Callback method to return an instance of a model.
     *
     * @return object[] An array of models.
     */
    function translateArray(dataSet, modelInstanceCallback) {
        var modelSet = [];
        for (var rowIndex in dataSet) {
            // Translate this row into a model and add it to the list.
            modelSet.push(translate(dataSet[rowIndex], modelInstanceCallback()));
        }
        return modelSet;
    }
    
    ObjectTranslator.prototype = {
        translate: translate,
        translateArray: translateArray
    }
    
    return ObjectTranslator;
}());

ticktack.factory("UsersApi", function ($q, $http, $injector) {
    // @TODO: This is a very crude login mechanism. We need to use OAUTH and tokens, etc.
    function _login(username, password) {
        var d = $q.defer();
        
        setTimeout(function () {
            $http({
                method: 'get',
                url: 'api/v1/login',
                params: {
                    username: username,
                    password: password
                }
            }).then(
                function success(response) {
                    // Response came back okay. See if it says the credentials were valid.
                    if (response.data.success) {
                        var userSession = $injector.get('LoggedInUser');
                        userSession.setUser(username, password);
                        d.resolve(userSession);
                    } else {
console.log('Login was rejected');
                        d.reject();
                    }
                },
                function failure(response) {
console.log('Login failed');
                    // Response failed for some reason. Assume the credentials are not valid.
                    // @TODO: We should tell the user and handle it better.
                    d.reject();
                }
            );
        }, 100);
        return d.promise
    }
    return { login: _login };
});

ticktack.factory('allProjects', function ($rootScope, $http, projectModel) {
    
    var projectList = null;
    
    // Method to call the API endpoint to get all the projects.
    var _refresh = function _refresh() {
        $http({
            method: "get",
            url: "api/v1/project",
        }).then(
            function success(response) {
                if (response.data.success) {
                    var objectTranslator = new ticktack.ObjectTranslator();
                    projectList = objectTranslator.translateArray(response.data.projects, function() { return new projectModel(); });
                }
            },
            function failure(response) {
                alert('Failed to load the projects.');
            }
        );
    }
    
    // Method to return all the projects.
    var _get = function _get() {
        return projectList;
    }
    
    return {
        refresh: _refresh,
        getAllProjectsData: _get
    };
    
});

ticktack.factory('allUsers', function ($rootScope, $http, userModel) {
    
    var userList = null;
    
    // Method to call the API endpoint to get all the users.
    var _refresh = function _refresh() {
        $http({
            method: "get",
            url: "api/v1/user",
        }).then(
            function success(response) {
                if (response.data.success) {
                    var objectTranslator = new ticktack.ObjectTranslator();
                    userList = objectTranslator.translateArray(response.data.users, function() { return new userModel(); });
                }
            },
            function failure(response) {
                alert('Failed to load the users.');
            }
        );
    }
    
    // Method to return all the users.
    var _get = function _get() {
        return userList;
    }
    
    return {
        refresh: _refresh,
        getAllUsersData: _get
    };
    
});

ticktack.service('loginModal', function ($modal, $rootScope) {
    
    function assignCurrentUser(user) {
        $rootScope.currentUser = user;
        return user;
    }

    return function() {
        var instance = $modal.open({
            templateUrl: 'views/loginModalTemplate.html',
            controller: 'LoginModalCtrl',
            controllerAs: 'LoginModalCtrl'
        })

        return instance.result.then(assignCurrentUser);
    };

});

ticktack.factory('referenceData', function ($rootScope) {
    
    var referenceData = null;
    
    // Method to set the entire collection of reference data.
    var _set = function _set(refData) {
        this.referenceData = refData;
    }
    
    // Method to return all the reference data
    var _get = function _get() {
        return this.referenceData;
    }
    
    // Method to return all the codes/descriptions for one type.
    var _getCodes = function _getCodes(refType) {
        if (this.referenceData.hasOwnProperty(refType)) {
            return this.referenceData[refType];
        }
        return false;
    }
    
    // Method to return the description for the given type and index.
    var _getDesc = function _getDesc(refType, refCode) {
        if (this.referenceData.hasOwnProperty(refType)) {
            for( var rdIdx = 0 ; rdIdx < this.referenceData[refType].length ; rdIdx++ ) {
                if (this.referenceData[refType][rdIdx].code == refCode) {
                    return this.referenceData[refType][rdIdx].desc;
                }
            }
        }
        return false;
    }
    
    return {
        setReferenceData: _set,
        getReferenceData: _get,
        getCodes: _getCodes,
        getDesc:_getDesc
    };
    
});

ticktack.controller('LoginModalCtrl', function ($scope, $timeout, $injector, UsersApi) {

    var userSession = $injector.get('LoggedInUser');
    var userCredentials = userSession.getUser();
    $scope.loginUsername = userCredentials.username;
    $scope.loginPassword = userCredentials.password;
    
    this.loginIsValid = true;
    
    this.cancel = $scope.$dismiss;

    this.submit = function (username, password) {
        var self = this;
        UsersApi.login(username, password)
            .then(function (user) {
                // Login was valid.
                this.loginIsValid = true;
                $scope.$close(user);
            }, function() {
                // Login wasn't valid.
                self.loginIsValid = false;
                $timeout(function() {
                    self.loginIsValid = true;
                }, 2000);
            });
    };

});

ticktack.controller('ProjectEditCtrl', ['$scope', '$http', '$stateParams', '$location', 'allProjects', 'projectModel', function ($scope, $http, $stateParams, $location, allProjects, projectModel) {
    
    // This controller is used for both creating and editing projects.
    $scope.project = {};
    
    $scope.projectIsLoaded = false;
    
    // Determine whether the user is creating a new project or editing one.
    $scope.mode = ('id' in $stateParams) ? 'update' : 'insert';
    
    // Load the project into the client.
    if ($scope.mode == 'update') {
        $http({
            method: "get",
            url: "api/v1/project/" + $stateParams.id,
        }).then(
            function success(response) {
                var objectTranslator = new ticktack.ObjectTranslator();
                $scope.project = objectTranslator.translate(response.data.projects[0], new projectModel());
                $scope.projectIsLoaded = true;  // User can save it if necessary.
            },
            function failure(response) {
                alert('Failed to load the project. Click OK to see the list of projects');
                $location.path('/projects');
            }
        );
    }
    
    $scope.saveProject = function saveProject() {
        if ($scope.projectIsLoaded || $scope.mode == 'insert') {
            if ($scope.editProject.$valid) {
                // Save the project.
                var saveVerb = ($scope.mode == 'update' ? 'put' : 'post');
                var saveUrl = "api/v1/project" + ($scope.mode == 'update' ? ('/' + $stateParams.id) : '');
                $http({
                    method: saveVerb,
                    url: saveUrl,
                    data: {
                        'name': $scope.project.name,
                        'description': $scope.project.description
                    }
                }).then(
                    function success(response) {
                        alert('Project was saved');
                        allProjects.refresh(); // Refresh the list of projects for the droplist in the ticket.
                        $location.path('/projects');
                    },
                    function failure(response) {
                        var message = 'Unknown error';
                        if (response.data) {
                            if (response.data.message) {
                                message = response.data.message;
                            }
                        }
                        alert('Save failed. Response: ' + message);
                        console.log('Failure response: ', response); // Full details
                    }
                );
            } else {
                alert('There are errors in the form (eg. min/max field length)');
            }
        } else {
            alert('Please wait for the project to load...');
        }
    }
    
    $scope.getModeDesc = function getModeDesc() {
        return $scope.mode == 'insert' ? 'New' : 'Edit';
    }
    
}]);


ticktack.controller('ProjectViewCtrl', ['$scope', '$http', '$stateParams', '$injector', 'referenceData', 'allUsers', 'ticketModel', 'projectModel', function ($scope, $http, $stateParams, $injector, referenceData, allUsers, ticketModel, projectModel) {
    
    // This controller is used for viewing a project.
    $scope.projectId = $stateParams.id;
    $scope.project = {};
    $scope.projectTickets = null;
    $scope.ticketStatuses = referenceData.getCodes('ticket_statuses');
    $scope.ticketPriorities = referenceData.getCodes('ticket_priorities'),
    
    allUsers.refresh();
    
    // Load the project into the client.
    $http({
        method: "get",
        url: "api/v1/project/" + $scope.projectId,
    }).then(
        function success(response) {
            var objectTranslator = new ticktack.ObjectTranslator();
            $scope.project = objectTranslator.translate(response.data.projects[0], new projectModel());
        },
        function failure(response) {
            alert('Failed to load the project. Click OK to see the list of projects');
            $location.path('/projects');
        }
    );
    
    // @TODO: I'm sure we can encapsulate this in a service, and just pass in
    // @TODO: the method, url and data, and pass stuff back, etc..
    $scope.getProjectTickets = function getProjectTickets() {
        $http({
            method: "get",
            url: "api/v1/project/" + $scope.projectId + "/ticket",
        }).then(
            function success(response) {
                var objectTranslator = new ticktack.ObjectTranslator();
                $scope.projectTickets = objectTranslator.translateArray(response.data.tickets, function() { return new ticketModel(); });
            },
            function failure(response) {
                console.log('Failure response: ', response);
            }
        );
    };
    
    $scope.getProjectTickets();
    
    $scope.getLoggedInUserId = function getLoggedInUserId() {
        var userSession = $injector.get('LoggedInUser');
        return userSession.getLoggedInUserId(allUsers.getAllUsersData());
    }
    
    // Method to return a list of tickets that match the filter.
    // Note: the ticket list is already filtered by tickets in this project.
    $scope.ticketFilter = function ticketFilter(filter) {
        // If we haven't loaded the tickets for this project yet, just return nothing.
        if ($scope.projectTickets == null) {
            return null;
        }
        var matches = [];
        for( var ticketIndex = 0 ; ticketIndex < $scope.projectTickets.length ; ticketIndex++ ) {
            if ($scope.projectTickets[ticketIndex].matches(filter)) {
                matches.push($scope.projectTickets[ticketIndex]);
            }
        }
        return matches;
    }
    
    $scope.countTicketFilter = function countTicketFilter(filter) {
        // If we haven't loaded the tickets for this project yet, just return nothing.
        if ($scope.projectTickets == null) {
            return null;
        }
        // Okay, the project tickets are available. Find all that match the filter, and return the count
        // of the result.
        var matchingTickets = $scope.ticketFilter(filter);
        return matchingTickets.length;
    }
    
}]);


ticktack.controller('ProjectsCtrl', function ($scope, $http, projectModel) {
    
    $scope.projectFilter = null;
    $scope.projects = [];
    $scope.loading = true;
    
    $scope.isLoading = function isLoading() {
        return $scope.loading;
    }
    
    // @TODO: I'm sure we can encapsulate this in a service, and just pass in
    // @TODO: the method, url and data, and pass stuff back, etc..
    // @TODO: Do this for all $http interactions.
    $scope.getProjects = function getProjects() {
        $scope.loading = true;
        var filter = '';
        if ($scope.projectFilter != null) {
            filter = '?description=' + $scope.projectFilter;
        }
        $http({
            method: "get",
            url: "api/v1/project" + filter,
        }).then(
            function success(response) {
                $scope.loading = false;
                var objectTranslator = new ticktack.ObjectTranslator();
                $scope.projects = objectTranslator.translateArray(response.data.projects, function() { return new projectModel(); });
            },
            function failure(response) {
                $scope.loading = false;
                console.log('Failure response: ', response);
            }
        );
    };
    $scope.getProjects();
    
});


ticktack.controller('RegisterCtrl', function ($scope, $http, $location, UsersApi) {
    
    $scope.username = null;
    $scope.password = null;
    $scope.confirm_password = null;
    
    $scope.errorMessage = [];
    
    // @TODO: I'm sure we can encapsulate this in a service, and just pass in
    // @TODO: the method, url and data, and pass stuff back, etc..
    $scope.registerUser = function registerUser() {
        // Do some basic validation.
        $scope.errorMessage = [];
        if ($scope.password != $scope.confirm_password) {
            $scope.errorMessage = ['Passwords must match'];
        } else {
            $http({
                method: "post",
                url: "api/v1/user",
                data: {
                    username: $scope.username,
                    password: $scope.password,
                    confirm_password: $scope.confirm_password
                }
            }).then(
                function success(response) {
                    if (response.data.success) {
                        // Registration was successful. Log the user in, then redirect
                        // to the projects list.
                        UsersApi
                            .login($scope.username, $scope.password)
                            .then(function() {
                                alert('Registration was successful');
                                $location.path('/projects');
                            })
                    } else {
                        $scope.errorMessage = response.data.errors;
                    }
                },
                function failure(response) {
                    console.log('Failure response: ', response);
                }
            );
        }
    };
    
});


ticktack.controller('TicketViewCtrl', ['$scope', '$http', '$stateParams', '$location', '$timeout', '$injector', 'referenceData', 'allProjects', 'allUsers', 'ticketModel', 'assignmentModel', 'relationshipModel', 'commentModel', 'timeSpentModel', function ($scope, $http, $stateParams, $location, $timeout, $injector, referenceData, allProjects, allUsers, ticketModel, assignmentModel, relationshipModel, commentModel, timeSpentModel) {
    
    // Make sure we have all the projects and all the users.
    allProjects.refresh();
    allUsers.refresh();
    
    // This controller is used for viewing a ticket. It is now also used for creating a new ticket
    $scope.ticket_id = $stateParams.hasOwnProperty('id') ? $stateParams.id : null;
    $scope.ticket = null;
    $scope.editingTicket = false;
    
    $scope.adding = {
        assignment: false,
        relationship: false,
        comment: false,
        timeSpent: false
    };
    // A spinner is shown when any of these values is greater than 1.
    $scope.spinners = {
        ticket: 0,
        assignment: 0,
        relationship: 0,
        comment: 0,
        timeSpent: 0
    };
    
    $scope.staticData = {
        types: referenceData.getCodes('ticket_types'),
        priorities: referenceData.getCodes('ticket_priorities'),
        statuses: referenceData.getCodes('ticket_statuses')
    }
    
    $scope.projectData = {};
    $scope.userData = {};
    
    // @TODO: These codes need to come from the database.
    $scope.relationshipTypes = [
        {code: "associated", description: "associated with"},
        {code: "dependent", description: "a dependency of"},
        {code: "dependency", description: "dependent upon"},
        {code: "parent", description: "a child of"},
        {code: "child", description: "a parent of"}
    ];
    $scope.newRelationship = {
        relType: null,
        trgTicket: null
    };
    // Model for a new comment.
    $scope.newComment = {
        detail: null
    };
    // Model for a new time spent record.
    $scope.newTimeSpent = {
        startDatetime: null,
        duration: null,
        notes: null
    };
    
    /**
     * Method to load the ticket and associated data, or just one piece of associated data,
     * into the controller.
     * @param string whichPart Which part of the data to load; pass null to load everything.
     */
    $scope.reloadTicketData = function reloadTicketData(whichPart) {
        // If we have a ticket, load its data.
        if ($scope.ticket_id != null) {
            // Call the API endpoint to get all the data.
            $http({
                method: "get",
                url: "api/v1/ticket/" + $scope.ticket_id,
            }).then(
                function success(response) {
                    // Now set the model appropriate to which part we are loading.
                    // If the data has not been loaded at all, override the parameter and assume "all".
                    var objectTranslator = new ticktack.ObjectTranslator();
                    if ($scope.ticket == null) {
                        $scope.ticket = objectTranslator.translate(response.data.tickets[0], new ticketModel());
                    } else {
                        switch (whichPart) {
                            case 'assignments':
                                $scope.ticket.assignments = objectTranslator.translateArray(response.data.tickets[0].assignments, function() { return new assignmentModel(); });
                                break;
                            case 'associated':
                                $scope.ticket.associated = objectTranslator.translateArray(response.data.tickets[0].associated, function() { return new relationshipModel(); });
                                break;
                            case 'comments':
                                $scope.ticket.comments = objectTranslator.translateArray(response.data.tickets[0].comments, function() { return new commentModel(); });
                                break;
                            case 'timeSpent':
                                $scope.ticket.timesSpent = objectTranslator.translateArray(response.data.tickets[0].timesSpent, function() { return new timeSpentModel(); });
                                // Update the totals in the ticket model.
                                objectTranslator.translate(response.data.tickets[0], $scope.ticket);
                                break;
                            default:
                                var objectTranslator = new ticktack.ObjectTranslator();
                                $scope.ticket = objectTranslator.translate(response.data.tickets[0], new ticketModel());
                                break;
                        }
                    }
                    // We seem to need to do this because the checkbox requires a true/false value.
                    for (var aidx = 0 ; aidx < $scope.ticket.associated.length ; aidx++ ) {
                        $scope.ticket.associated[aidx].pinned = ($scope.ticket.associated[aidx].pinned == '1');
                    }
console.log('Time spent object:', $scope.ticket.timeSpentTotals);
                },
                function failure(response) {
                    alert('Failed to load the ticket. Click OK to see the list of tickets');
                    $location.path('/tickets');
                }
            );
        }
    }
    
    // Method to load all the tickets in the system, that the logged-in user can see.
    // @TODO: This needs to be done as a service.
    $scope.loadAllTickets = function loadAllTickets() {
        $http({
            method: "get",
            url: "api/v1/ticket",
        }).then(
            function success(response) {
                if (response.data.success) {
                    var objectTranslator = new ticktack.ObjectTranslator();
                    $scope.allTickets = objectTranslator.translateArray(response.data.tickets, function() { return new ticketModel(); });
                }
            },
            function failure(response) {
                alert('Failed to load all tickets.');
            }
        );
    }
    
    $scope.startWork = function startWork(whichSection) {
        if ($scope.spinners.hasOwnProperty(whichSection)) {
            $scope.spinners[whichSection]++;
        }
    }
    
    $scope.finishWork = function startWork(whichSection) {
        if ($scope.spinners.hasOwnProperty(whichSection)) {
            if ($scope.spinners[whichSection] > 0) {
                $scope.spinners[whichSection]--;
            }
        }
    }
    
    $scope.toggle = function toggle(which) {
        $scope.adding[which] = !$scope.adding[which];
        if (which == 'assignment') {
            $scope.populateAssigneeList();
        }
        if (which == 'timeSpent') {
            var tmpDate = new Date();
            $scope.newTimeSpent.startDatetime = tmpDate.toLocaleDateString() + ' ' + tmpDate.toLocaleTimeString();
            $scope.newTimeSpent.startDatetime = $scope.newTimeSpent.startDatetime.substr(0, 16);
        }
    }
    
    $scope.populateAssigneeList = function populateAssigneeList() {
        $scope.assignees = allUsers.getAllUsersData();
        if ($scope.assignees == null) {
            $scope.assignees = [];
        }
        // For some reason, it remembers this dummy row, so we have to check it's not there before we add it.
        if ($scope.assignees.length == 0 || $scope.assignees[0].id != -1) {
            $scope.assignees.unshift({
                id: -1,
                userName: 'Please select...'
            });
        }
    }
    
    $scope.populateAssigneeList();
    $scope.newAssignee = {id: $scope.assignees[0].id};
    
    // Assignments
    
    // User wants to create a new assignment.
    $scope.saveAssignment = function saveAssignment() {
        if ($scope.newAssignee.id > 0) {
            $scope.startWork('assignment');
            // Send the new assignment to the server.
            $http({
                method: "post",
                url: "api/v1/ticket/" + $scope.ticket_id + '/assignment/' + $scope.newAssignee.id,
            }).then(
                function success(response) {
                    if (response.data.success) {
                        // Assignment was successful. Load all the assignments back into memory.
                        $scope.reloadTicketData('assignments');
                        // Set the selection to the blank option and close the form.
                        $scope.newAssignee.id = $scope.assignees[0].id;
                        $scope.toggle('assignment');
                    }
                    $scope.finishWork('assignment');
                },
                function failure(response) {
                    alert('There was a problem saving this assignment: ' + response.data.message);
                    $scope.finishWork('assignment');
                }
            );
        }
    }
    
    $scope.unassign = function unassign(assignmentId) {
        var confirmDelete = confirm('Are you sure you want to remove this assignment?');
        if (!confirmDelete) {
            return;
        }
        $scope.startWork('assignment');
        $http({
            method: 'delete',
            url: 'api/v1/assignment/' + assignmentId
        }).then(
            function success(response) {
                if (response.data.success) {
                    // Delete was successul. Load all the assignments back into memory.
                    $scope.reloadTicketData('assignments');
                }
                $scope.finishWork('assignment');
            },
            function failure(response) {
                $scope.finishWork('assignment');
            }
        )
    }
    
    $scope.moveAssignment = function moveAssignment(assignmentId, dir) {
        if (dir == 'up' || dir == 'down') {
            $scope.startWork('assignment');
            $http({
                method: 'put',
                url: 'api/v1/assignment/' + assignmentId + '/' + dir
            }).then(
                function success(response) {
                    if (response.data.success) {
                        // Movement was successul. Load all the assignments back into memory.
                        $scope.reloadTicketData('assignments');
                    }
                    $scope.finishWork('assignment');
                },
                function failure(response) {
                    console.log('Failure. Response...', response);
                    $scope.finishWork('assignment');
                }
            )
        } else {
            alert('Unknown direction: ' + dir);
        }
    }
    
    // Relationships
    
    // Adding a new association.
    $scope.saveRelationship = function saveRelationship() {
        if ($scope.newRelationship.relType == null) {
            alert('You must choose a relationship type');
            return false;
        }
        if ($scope.newRelationship.trgTicket == null) {
            alert('You must choose a ticket');
            return false;
        }
        $scope.startWork('relationship');
        // Send the new relationship to the server.
        $http({
            method: "post",
            url: 'api/v1/ticket/' + $scope.ticket_id + '/associated',
            data: {
                relationship_type: $scope.newRelationship.relType.code,
                target_ticket_id: $scope.newRelationship.trgTicket.id,
                is_pinned: false
            }
        }).then(
            function success(response) {
                if (response.data.success) {
                    // Save was successful. Load all the relationships back into memory.
                    $scope.reloadTicketData('relationships');
                    // Set the selection to the blank options and close the form.
                    $scope.newRelationship.relType = null;
                    $scope.newRelationship.trgTicket = null;
                    $scope.toggle('relationship');
                }
                $scope.finishWork('relationship');
            },
            function failure(response) {
                alert('There was a problem saving this relationship: ' + response.data.message);
                $scope.finishWork('relationship');
            }
        );
    }
    
    // Toggle the pinned state of a relationship.
    $scope.togglePinned = function togglePinned(associationRow) {
        $scope.startWork('relationship');
        var pinAction = associationRow.pinned ? 'pin' : 'unpin';
        $http({
            method: 'put',
            url: 'api/v1/ticket/' + $scope.ticket.id + '/associated/' + associationRow.id + '/' + pinAction
        }).then(
            function success(response) {
                if (response.data.success) {
                    // Pin/unpin was successul. Load all the associations back into memory.
                    $scope.reloadTicketData('associated');
                }
                $scope.finishWork('relationship');
            },
            function failure(response) {
                $scope.finishWork('relationship');
            }
        )
    }
    
    // Move an association up/down.
    $scope.moveAssociated = function moveAssociated(associatedId, dir) {
        if (dir == 'up' || dir == 'down') {
            $scope.startWork('relationship');
            $http({
                method: 'put',
                url: 'api/v1/ticket/' + $scope.ticket.id + '/associated/' + associatedId + '/' + dir
            }).then(
                function success(response) {
                    if (response.data.success) {
                        // Movement was successul. Load all the associations back into memory.
                        $scope.reloadTicketData('associated');
                    }
                    $scope.finishWork('relationship');
                },
                function failure(response) {
                    $scope.finishWork('relationship');
                }
            )
        } else {
            alert('Unknown direction: ' + dir);
        }
    }
    
    // Delete an association.
    $scope.deleteAssociated = function deleteAssociated(associatedId) {
        if (confirm('Are you sure you want to delete this relationship?')) {
            $scope.startWork('relationship');
            $http({
                method: 'delete',
                url: 'api/v1/ticket/' + $scope.ticket.id + '/associated/' + associatedId
            }).then(
                function success(response) {
                    if (response.data.success) {
                        // Delete was successul. Load all the associations back into memory.
                        $scope.reloadTicketData('associated');
                    }
                    $scope.finishWork('relationship');
                },
                function failure(response) {
                    $scope.finishWork('relationship');
                }
            )
        }
    }
    
    // Convert the relationship type to a description.
    // @TODO: When the relationship types come from the DB, this method should be inside the Relationship model.
    $scope.relationshipTypeToDesc = function relationshipTypeToDesc(relType) {
        for(var relIndex in $scope.relationshipTypes) {
            if ($scope.relationshipTypes[relIndex].code == relType) {
                return $scope.relationshipTypes[relIndex].description;
            }
        }
        return '[Unknown]';
    }
    
    // Comments
    
    // Method to save a comment.
    $scope.saveComment = function saveComment() {
        if ($scope.newComment.detail) {
            $scope.startWork('comment');
            // Send the new comment to the server.
            $http({
                method: "post",
                url: 'api/v1/ticket/' + $scope.ticket_id + '/comment',
                data: {
                    details: $scope.newComment.detail
                    // "Added by" will default to the logged-in user.
                }
            }).then(
                function success(response) {
                    if (response.data.success) {
                        // Save was successful. Load all the comments back into memory.
                        $scope.reloadTicketData('comments');
                        // Set the selection to the blank options and close the form.
                        $scope.newComment.detail = null;
                        $scope.toggle('comment');
                    }
                    $scope.finishWork('comment');
                },
                function failure(response) {
                    alert('There was a problem saving your comment: ' + response.data.message);
                    $scope.finishWork('comment');
                }
            );
        } else {
            alert('You must actually enter a comment!');
        }
    }
    
    // Time Spent records.
    
    // Method to save a new time spent record.
    $scope.saveTimeSpent = function saveTimeSpent() {
        if ($scope.newTimeSpent.startDatetime &&
            $scope.newTimeSpent.duration &&
            $scope.newTimeSpent.notes) {
            $scope.startWork('timeSpent');
            // Send the new time spent record to the server.
            $http({
                method: "post",
                url: 'api/v1/ticket/' + $scope.ticket_id + '/time_spent',
                data: {
                    start_datetime: $scope.newTimeSpent.startDatetime,
                    duration: $scope.newTimeSpent.duration,
                    notes: $scope.newTimeSpent.notes
                    // "User" will default to the logged-in user.
                }
            }).then(
                function success(response) {
                    if (response.data.success) {
                        // Save was successful. Load all the time spent records back into memory.
                        $scope.reloadTicketData('timeSpent');
                        // Set the selection to the blank options and close the form.
                        $scope.newTimeSpent.startDatetime = null;
                        $scope.newTimeSpent.duration = null;
                        $scope.newTimeSpent.notes = null;
                        $scope.toggle('timeSpent');
                    }
                    $scope.finishWork('timeSpent');
                },
                function failure(response) {
                    alert('There was a problem recording your time: ' + response.data.message);
                    $scope.finishWork('timeSpent');
                }
            );
        } else {
            alert('You must enter the start time, duration and notes!');
        }
    }
    
    // Ticket editing methods.
    $scope.toggleEdit = function toggleEdit(newState) {
        if (newState == undefined) {
            $scope.editingTicket = !$scope.editingTicket;
        } else {
            $scope.editingTicket = newState;
        }
        if ($scope.editingTicket) {
            // We're editing a ticket - make sure the project- and user lists are up to date.
            $scope.projectData = allProjects.getAllProjectsData();
            $scope.userData = allUsers.getAllUsersData();
        } else {
            if ($scope.ticket_id == null) {
                // They've canceled an insert, so jump back to the ticket list.
                $location.path('/tickets');
            }
        }
    }
    
    if ($scope.ticket_id == null) {
        $timeout(function() {
            $scope.projectData = allProjects.getAllProjectsData();
            $scope.projectData.unshift({
                id: -1,
                name: 'Please select...'
            });
            $scope.userData = allUsers.getAllUsersData();
            // If created by user is not set, find out who is currently logged in, and set it to that.
            if ($scope.ticket.created_by_user_id == null) {
                var userSession = $injector.get('LoggedInUser');
                $scope.ticket.setCreatedByUserId(userSession.getLoggedInUserId($scope.userData));
                // If there's still no selection, pick the first one!
                if ($scope.ticket.getCreatedByUserId() == null) {
                    $scope.ticket.setCreatedByUserId($scope.userData[0].id);
                }
            }
        }, 1000);
        $scope.ticket = new ticketModel();
        $scope.toggleEdit();
    }
    
    // Save the ticket.
    $scope.saveTicket = function saveTicket() {
        // @TODO: Do any validation here.
        // Save the ticket.
        $scope.startWork('ticket');
        var saveMethod = $scope.ticket_id == null ? 'post' : 'put';
        var saveUrl = 'api/v1/ticket' + ($scope.ticket_id == null ? '' : '/' + $scope.ticket.id);
        $http({
            method: saveMethod,
            url: saveUrl,
            data: {
                // @TODO: Do We need getters for all these fields?
                project_id: $scope.ticket.projectId,
                ticket_type: $scope.ticket.ticketType,
                summary: $scope.ticket.summary,
                description: $scope.ticket.description,
                created_by_user_id: $scope.ticket.createdByUserId,
                priority: $scope.ticket.priority,
                status: $scope.ticket.status,
                due_datetime: $scope.ticket.dueDatetimeForView,
                estimated_length_seconds: $scope.ticket.estimatedLengthForView
            }
        }).then(
            function success(response) {
                if (response.data.success) {
                    // Save ticket was successul. Load the ticket back into memory.
                    if ($scope.ticket_id == null) {
                        $location.path('tickets/' + response.data.id);
                    } else {
                        $scope.reloadTicketData('ticket');
                        $scope.toggleEdit();
                    }
                }
                $scope.finishWork('ticket');
            },
            function failure(response) {
                alert(response.data.message);
                $scope.finishWork('ticket');
            }
        )
    }
    
    // Load all the data for this ticket.
    $scope.reloadTicketData();
    
    // Load the basic info for all tickets.
    // @TODO: This needs to be a service that runs once at startup, and maybe periodically (or after tickets are edited).
    $scope.loadAllTickets();
    
    $scope.codeToDesc = function codeToDesc(refType, refValue) {
        return referenceData.getDesc(refType, refValue);
    }
    
}]);


ticktack.controller('TicketsCtrl', ['$scope', '$http', '$stateParams', '$location', 'allProjects', 'allUsers', 'referenceData', 'ticketModel', function ($scope, $http, $stateParams, $location, allProjects, allUsers, referenceData, ticketModel) {
    
    $scope.tickets = [];
    $scope.projectData = [];
    $scope.userData = [];
    $scope.filter = {};
    $scope.filterFirstTime = true;
    $scope.loading = true;
    
    $scope.isLoading = function isLoading() {
        return $scope.loading;
    }
    
    $scope.staticData = {
        types: referenceData.getCodes('ticket_types'),
        priorities: referenceData.getCodes('ticket_priorities'),
        statuses: referenceData.getCodes('ticket_statuses')
    }
    
    $scope.filterIsVisible = false; // Hide the ticket filter by default.
    
    // Make sure we have all the projects and users.
    allProjects.refresh();
    allUsers.refresh();
    
    // @TODO: I'm sure we can encapsulate this in a service, and just pass in
    // @TODO: the method, url and data, and pass stuff back, etc..
    $scope.getTickets = function getTickets() {
        $scope.loading = true;
        // Note: statuses, priorities and assignment user ids can be arrays, and they are implicitly
        // converted to a CSL here.
        var filter = [];
        if ($stateParams.summary != null) {
            filter.push('summary=' + $stateParams.summary);
        }
        if ($stateParams.description != null) {
            filter.push('description=' + $stateParams.description);
        }
        if ($stateParams.projectId != null) {
            filter.push('project_id=' + $stateParams.projectId);
        }
        if ($stateParams.ticketTypes != null) {
            filter.push('ticket_types=' + $stateParams.ticketTypes);
        }
        if ($stateParams.createdByUserId != null) {
            filter.push('created_by_user_id=' + $stateParams.createdByUserId);
        }
        if ($stateParams.statuses != null) {
            filter.push('statuses=' + $stateParams.statuses);
        }
        if ($stateParams.priorities != null) {
            filter.push('priorities=' + $stateParams.priorities);
        }
        if ($stateParams.assignmentUserIds != null) {
            filter.push('assignment_user_ids=' + $stateParams.assignmentUserIds);
        }
        if ($stateParams.assignmentLevel != null) {
            filter.push('assignment_level=' + $stateParams.assignmentLevel);
        }
        $http({
            method: "get",
            url: "api/v1/ticket" + (filter.length > 0 ? '?' + filter.join('&') : ''),
        }).then(
            function success(response) {
                $scope.loading = false;
                var objectTranslator = new ticktack.ObjectTranslator();
                $scope.tickets = objectTranslator.translateArray(response.data.tickets, function() { return new ticketModel(); });
            },
            function failure(response) {
                $scope.loading = false;
                console.log('Failure response: ', response);
            }
        );
    };
    
    $scope.clearFilter = function clearFilter(optionalEvent) {
        if (optionalEvent != undefined) {
            optionalEvent.preventDefault();
        }
        $scope.filter = {
            summary: null,
            description: null,
            projectId: null,
            type: {},
            createdByUserId: null,
            priority: {},
            status: {},
            assignment: {
                userId: null,
                level: ''
            }
        };
        for(var typeIndex = 0 ; typeIndex < $scope.staticData.types.length ; typeIndex++) {
            $scope.filter.type[$scope.staticData.types[typeIndex].code] = false;
        }
        for(var priorityIndex = 0 ; priorityIndex < $scope.staticData.priorities.length ; priorityIndex++) {
            $scope.filter.priority[$scope.staticData.priorities[priorityIndex].code] = false;
        }
        for(var statusIndex = 0 ; statusIndex < $scope.staticData.priorities.length ; statusIndex++) {
            $scope.filter.status[$scope.staticData.statuses[statusIndex].code] = false;
        }
    }
    
    $scope.populateFilter = function populateFilter() {
        if ($stateParams.summary != null) {
            $scope.filter.summary = $stateParams.summary;
        }
        if ($stateParams.description != null) {
            $scope.filter.description = $stateParams.description;
        }
        if ($stateParams.projectId != null) {
            $scope.filter.projectId = $stateParams.projectId;
        }
        if ($stateParams.ticketTypes != null) {
            $scope.setFilterOptionsToTrue('type', $stateParams.ticketTypes);
        }
        if ($stateParams.createdByUserId != null) {
            $scope.filter.createdByUserId = $stateParams.createdByUserId;
        }
        if ($stateParams.statuses != null) {
            $scope.setFilterOptionsToTrue('status', $stateParams.statuses);
        }
        if ($stateParams.priorities != null) {
            $scope.setFilterOptionsToTrue('priority', $stateParams.priorities);
        }
        if ($stateParams.assignmentUserIds != null) {
            $scope.filter.assignment.userId = $stateParams.assignmentUserIds;
        }
        if ($stateParams.assignmentLevel != null) {
            $scope.filter.assignment.level = $stateParams.assignmentLevel;
        }
    }
    
    $scope.toggleShowFilter = function toggleShowFilter() {
        $scope.filterIsVisible = !$scope.filterIsVisible;
        if ($scope.filterIsVisible) {
            // We're in the filter view - make sure the project- and user lists are up to date.
            if ($scope.filterFirstTime) {
                $scope.filterFirstTime = false;
                $scope.projectData = allProjects.getAllProjectsData();
                $scope.userData = allUsers.getAllUsersData();
            }
        }
    }
    
    $scope.applyFilter = function applyFilter() {
        // Build up the search options from the filter options.
        var options = {};
        if ($scope.filter.summary) {
            options.summary = $scope.filter.summary;
        }
        if ($scope.filter.description) {
            options.description = $scope.filter.description;
        }
        if ($scope.filter.projectId) {
            options.projectId = $scope.filter.projectId;
        }
        var typeOptions = $scope.getTrueFilterOptions($scope.filter.type);
        if (typeOptions != '') {
            options.ticketTypes = typeOptions;
        }
        if ($scope.filter.createdByUserId) {
            options.createdByUserId = $scope.filter.createdByUserId;
        }
        var priorityOptions = $scope.getTrueFilterOptions($scope.filter.priority);
        if (priorityOptions != '') {
            options.priorities = priorityOptions;
        }
        var statusOptions = $scope.getTrueFilterOptions($scope.filter.status);
        if (statusOptions != '') {
            options.statuses = statusOptions;
        }
        if ($scope.filter.assignment.userId) {
            options.assignmentUserIds = $scope.filter.assignment.userId;
            options.assignmentLevel = $scope.filter.assignment.level;
        }
        // And load the page.
        $location.path('/tickets').search(options);
    }
    
    /**
     * Method to return the key name of any property in the given object that has a value of true.
     */
    $scope.getTrueFilterOptions = function getTrueFilterOptions(filterObject) {
        var result = [];
        for(var filterProp in filterObject) {
            if (filterObject.hasOwnProperty(filterProp) && filterObject[filterProp] === true) {
                result.push(filterProp);
            }
        }
        return result.join(',');
    }
    
    /**
     * Method to take all the values in a CSL and set the property with the same name in the
     * given property of the filter object to true.
     * It seems that a newer version of Angular causes the CSL values to be passed in as an array
     * in some instances, so we now have to check for that.
     */
    $scope.setFilterOptionsToTrue = function setFilterOptionsToTrue(filterProperty, propertyCSL) {
        var propertyList = propertyCSL;
        if (!Array.isArray(propertyList)) {
            propertyList = propertyList.split(',');
        }
        if (propertyList.length > 0) {
            for(var propIndex = 0 ; propIndex < propertyList.length ; propIndex++) {
                $scope.filter[filterProperty][propertyList[propIndex]] = true;
            }
        }
    }
    
    $scope.getTickets();
    
    $scope.clearFilter();
    $scope.populateFilter();
    
}]);


ticktack.controller('TicktackCtrl', function ($scope, $rootScope) {
    
    $scope.isLoggedIn = function isLoggedIn() {
        return typeof $rootScope.currentUser !== 'undefined';
    }
    
    $scope.loggedInUser = function loggedInUser() {
        if ($scope.isLoggedIn()) {
            return $rootScope.currentUser.getUser().username;
        }
        return null;
    }
    
});
