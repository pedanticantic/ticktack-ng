'use strict';

// Declare app level module which depends on views, and components
var ticktack = angular.module('ticktack', [
    'ui.router',
    'ui.bootstrap',
    'ngResource'
]);

/* This is to do with intercepting a 401 coming from the server - it means the
   user was logged in, but isn't now.
ticktack.config(function ($httpProvider) {

    $httpProvider.interceptors.push(function ($timeout, $q, $injector) {
        var loginModal, $http, $state;

        // this trick must be done so that we don't receive
        // `Uncaught Error: [$injector:cdep] Circular dependency found`
        $timeout(function () {
            loginModal = $injector.get('loginModal');
            $http = $injector.get('$http');
            $state = $injector.get('$state');
        });

        return {
            responseError: function (rejection) {
                if (rejection.status !== 401) {
                    return rejection;
                }

                var deferred = $q.defer();

                loginModal()
                    .then(function () {
                        deferred.resolve( $http(rejection.config) );
                    })
                    .catch(function () {
                        $state.go('welcome');
                        deferred.reject(rejection);
                    });

                return deferred.promise;
            }
        };
    });

});
*/

// For API calls, we need to convert the app host to the server host and add
// the auth headers.
ticktack.config(function ($httpProvider) {
    $httpProvider.interceptors.push(function ($q, $location, $rootScope) {
        return {
            'request': function (config) {
                if (config.url.substr(0, 4) == 'api/') {
                    var serverHost = $location.$$host;
                    switch (serverHost) {
                        case 'ticktack.ng.dev':
                            serverHost = 'http://ticktack.dev:8088/';
                            break;
                    }
                    config.url = serverHost + config.url;
                    // Sort out the user credentials. Only if it's a call to the API
                    // and it's not a login request, and we have a valid user.
                    if ($rootScope.currentUser && config.url.indexOf('/login') == -1) {
                        config.headers.Authorization = $rootScope.currentUser.getAuthorization();
                    }
                }
                return config || $q.when(config);
            }
        }
    });
});

ticktack.filter('sanitise', ['$sce', function($sce) {
    return function(htmlCode){
        return $sce.trustAsHtml(htmlCode);
    }
}]);

ticktack.run(function ($rootScope, $state, $injector, $http, loginModal, UsersApi, referenceData) {

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
        var userSession = $injector.get('User');
        var userCredentials = userSession.getUser();
        if (toState.data.forgetLogin === true) {
            $rootScope.currentUser = undefined;
            userSession.setUser(undefined, undefined);
        } else {
            var requireLogin = toState.data.requireLogin;

            if (requireLogin && typeof $rootScope.currentUser === 'undefined') {
                event.preventDefault();
                
                // User isn't logged in. See if we still have their credentials. If so
                // auto-log them in. This happens if user refreshes the screen.
                if (userCredentials &&
                    userCredentials.username != 'undefined' &&
                    userCredentials.username != undefined &&
                    userCredentials.password != 'undefined' &&
                    userCredentials.password != undefined) {
                    UsersApi.login(userCredentials.username, userCredentials.password)
                        .then(function (user) {
                            $rootScope.currentUser = user;
                            $state.go(toState.name, toParams)
                        }, function() {
                            console.log('Something went wrong with auto login', userCredentials);
                        });
                } else {
                    
                    // User is not logged in and must have explicitly logged out. Show the login form.
                    loginModal()
                        .then(function () {
                            return $state.go(toState.name, toParams);
                        })
                        .catch(function () {
                            // User canceled the login modal. Go to the default page.
                            return $state.go('home');
                        });
                }
            }
        }
    });
    
    // Call the API endpoint to get all the static reference data.
    $http({
        method: "get",
        url: "api/v1/referenceData",
    }).then(
        function success(response) {
            if (response.data.success) {
                referenceData.setReferenceData(response.data.referenceData);
            }
        },
        function failure(response) {
            alert('Failed to load the reference data.');
            $location.path('/home');
        }
    );

});


ticktack.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');

    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'partials/home.html',
            // ...
            data: {
                requireLogin: false
            }
        })
        
        // Projects
        .state('projects', {
            url: '/projects',
            templateUrl: 'partials/projects.html',
            controller: 'ProjectsCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        .state('projectcreate', {
            url: '/projects/create',
            templateUrl: 'partials/project-edit.html',
            controller: 'ProjectEditCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        .state('projectview', {
            url: '/projects/:id',
            templateUrl: 'partials/project-view.html',
            controller: 'ProjectViewCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        .state('projectedit', {
            url: '/projects/:id/edit',
            templateUrl: 'partials/project-edit.html',
            controller: 'ProjectEditCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        
        // Tickets
        .state('tickets', {
            url: '/tickets',
            templateUrl: 'partials/tickets.html',
            controller: 'TicketsCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        .state('ticketview', {
            url: '/tickets/:id',
            templateUrl: 'partials/ticket-view.html',
            controller: 'TicketViewCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        .state('ticketadd', {
            url: '/ticket',
            templateUrl: 'partials/ticket-view.html',
            controller: 'TicketViewCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        
        // Logout
        .state('logout', {
            url: '/logout',
            templateUrl: 'partials/logout.html',
            // ...
            data: {
                forgetLogin: true
            }
        })
        .state('app', {
            abstract: true,
            // ...
            data: {
                requireLogin: true // this property will apply to all children of 'app'
            }
        })

});

ticktack.service('User', function () {
    
    var retrieveUser = function retrieveUser() {
        if (localStorage.username && localStorage.password) {
            user.username = localStorage.username;
            user.password = localStorage.password;
        }
    }
    
    var saveUser = function saveUser() {
        localStorage.username = user.username;
        localStorage.password = user.password;
    }
    
    /**
     * @var Underlying object holding the user details.
     */
    var user = {
        username: '',
        password: ''
    }
    retrieveUser();
    
    /**
     * Method to set the username and password in the internal variable.
     * @param username
     * @param password
     */
    function setUser(username, password) {
        user.username = username;
        user.password = password;
        saveUser();
    }
    
    /**
     * Method to return the internal variable.
     * @return user
     */
    function getUser() {
        var un = user.username;
        var pw = user.password;
        return {username: un, password: pw};
    }
    
    /**
     * Method to return the authorization string for this user.
     * @return string
     */
    function getAuthorization() {
        return 'Basic ' + window.btoa(user.username + ':' + user.password);
    }

    return {setUser: setUser, getUser: getUser, getAuthorization: getAuthorization};

});

ticktack.factory("UsersApi", function ($q, $http, $injector) {
    // @TODO: This is a very crude login mechanism. We need to use OAUTH and tokens, etc.
    function _login (username, password) {
        var d = $q.defer();
        
        setTimeout(function () {
            $http({
                method: 'get',
                url: 'api/v1/login',
                params: {
                    username: username,
                    password: password
                }
            }).then(
                function success(response) {
                    // Response came back okay. See if it says the credentials were valid.
                    if (response.data.success) {
                        var userSession = $injector.get('User');
                        userSession.setUser(username, password);
                        d.resolve(userSession);
                    } else {
console.log('Login was rejected');
                        d.reject();
                    }
                },
                function failure(response) {
console.log('Login failed');
                    // Response failed for some reason. Assume the credentials are not valid.
                    // @TODO: We should tell the user and handle it better.
                    d.reject();
                }
            );
        }, 100);
        return d.promise
    }
    return { login: _login };
});

ticktack.factory('allProjects', function ($rootScope, $http) {
    
    var projectList = null;
    
    // Method to call the API endpoint to get all the projects.
    var _refresh = function _refresh() {
        $http({
            method: "get",
            url: "api/v1/project",
        }).then(
            function success(response) {
                if (response.data.success) {
                    projectList = response.data.projects;
                }
            },
            function failure(response) {
                alert('Failed to load the projects.');
            }
        );
    }
    
    // Method to return all the projects.
    var _get = function _get() {
        return projectList;
    }
    
    return {
        refresh: _refresh,
        getAllProjectsData: _get
    };
    
});

ticktack.factory('allUsers', function ($rootScope, $http) {
    
    var userList = null;
    
    // Method to call the API endpoint to get all the users.
    var _refresh = function _refresh() {
        $http({
            method: "get",
            url: "api/v1/user",
        }).then(
            function success(response) {
                if (response.data.success) {
                    userList = response.data.users;
                }
            },
            function failure(response) {
                alert('Failed to load the users.');
            }
        );
    }
    
    // Method to return all the users.
    var _get = function _get() {
        return userList;
    }
    
    return {
        refresh: _refresh,
        getAllUsersData: _get
    };
    
});

ticktack.service('loginModal', function ($modal, $rootScope) {
    
    function assignCurrentUser(user) {
        $rootScope.currentUser = user;
        return user;
    }

    return function() {
        var instance = $modal.open({
            templateUrl: 'views/loginModalTemplate.html',
            controller: 'LoginModalCtrl',
            controllerAs: 'LoginModalCtrl'
        })

        return instance.result.then(assignCurrentUser);
    };

});

ticktack.factory('referenceData', function ($rootScope) {
    
    var referenceData = null;
    
    // Method to set the entire collection of reference data.
    var _set = function _set(refData) {
        this.referenceData = refData;
    }
    
    // Method to return all the reference data
    var _get = function _get() {
        return this.referenceData;
    }
    
    // Method to return all the codes/descriptions for one type.
    var _getCodes = function _getCodes(refType) {
        if (this.referenceData.hasOwnProperty(refType)) {
            return this.referenceData[refType];
        }
        return false;
    }
    
    // Method to return the description for the given type and index.
    var _getDesc = function _getDesc(refType, refCode) {
        if (this.referenceData.hasOwnProperty(refType)) {
            for( var rdIdx = 0 ; rdIdx < this.referenceData[refType].length ; rdIdx++ ) {
                if (this.referenceData[refType][rdIdx].code == refCode) {
                    return this.referenceData[refType][rdIdx].desc;
                }
            }
        }
        return false;
    }
    
    return {
        setReferenceData: _set,
        getReferenceData: _get,
        getCodes: _getCodes,
        getDesc:_getDesc
    };
    
});

ticktack.controller('LoginModalCtrl', function ($scope, $timeout, $injector, UsersApi) {

    var userSession = $injector.get('User');
    var userCredentials = userSession.getUser();
    $scope.loginUsername = userCredentials.username;
    $scope.loginPassword = userCredentials.password;
    
    this.loginIsValid = true;
    
    this.cancel = $scope.$dismiss;

    this.submit = function (username, password) {
        var self = this;
        UsersApi.login(username, password)
            .then(function (user) {
                // Login was valid.
                this.loginIsValid = true;
                $scope.$close(user);
            }, function() {
                // Login wasn't valid.
                self.loginIsValid = false;
                $timeout(function() {
                    self.loginIsValid = true;
                }, 2000);
            });
    };

});

ticktack.controller('ProjectEditCtrl', ['$scope', '$http', '$stateParams', '$location', 'allProjects', function ($scope, $http, $stateParams, $location, allProjects) {
    
    // This controller is used for both creating and editing projects.
    $scope.project_name = '';
    $scope.project_description = '';
    
    $scope.projectIsLoaded = false;
    
    // Determine whether the user is creating a new project or editing one.
    $scope.mode = ('id' in $stateParams) ? 'update' : 'insert';
    
    // Load the project into the client.
    if ($scope.mode == 'update') {
        $http({
            method: "get",
            url: "api/v1/project/" + $stateParams.id,
        }).then(
            function success(response) {
                var thisProject = response.data.projects[0];
                $scope.project_name = thisProject.name;
                $scope.project_description = thisProject.description;
                $scope.projectIsLoaded = true;  // User can save it if necessary.
            },
            function failure(response) {
                alert('Failed to load the project. Click OK to see the list of projects');
                $location.path('/projects');
            }
        );
    }
    
    $scope.saveProject = function saveProject() {
        if ($scope.projectIsLoaded || $scope.mode == 'insert') {
            if ($scope.editProject.$valid) {
                // Save the project.
                var saveVerb = ($scope.mode == 'update' ? 'put' : 'post');
                var saveUrl = "api/v1/project" + ($scope.mode == 'update' ? ('/' + $stateParams.id) : '');
                $http({
                    method: saveVerb,
                    url: saveUrl,
                    data: {
                        'name': $scope.project_name,
                        'description': $scope.project_description
                    }
                }).then(
                    function success(response) {
                        alert('Project was saved');
                        allProjects.refresh(); // Refresh the list of projects for the droplist in the ticket.
                        $location.path('/projects');
                    },
                    function failure(response) {
                        var message = 'Unknown error';
                        if (response.data) {
                            if (response.data.message) {
                                message = response.data.message;
                            }
                        }
                        alert('Save failed. Response: ' + message);
                        console.log('Failure response: ', response); // Full details
                    }
                );
            } else {
                alert('There are errors in the form (eg. min/max field length)');
            }
        } else {
            alert('Please wait for the project to load...');
        }
    }
    
    $scope.getModeDesc = function getModeDesc() {
        return $scope.mode == 'insert' ? 'New' : 'Edit';
    }
    
}]);


ticktack.controller('ProjectViewCtrl', ['$scope', '$http', '$stateParams', function ($scope, $http, $stateParams) {
    
    // This controller is used for viewing a project.
    $scope.project_name = '';
    $scope.project_description_markdown = '';
    $scope.project_id = $stateParams.id;
    
    // Load the project into the client.
    $http({
        method: "get",
        url: "api/v1/project/" + $scope.project_id,
    }).then(
        function success(response) {
            var thisProject = response.data.projects[0];
            $scope.project_name = thisProject.name;
            $scope.project_description_markdown = thisProject.description_markdown;
        },
        function failure(response) {
            alert('Failed to load the project. Click OK to see the list of projects');
            $location.path('/projects');
        }
    );
    
}]);


ticktack.controller('ProjectsCtrl', function ($scope, $http) {
    
    $scope.projectFilter = null;
    $scope.projects = [];
    
    // @TODO: I'm sure we can encapsulate this in a service, and just pass in
    // @TODO: the method, url and data, and pass stuff back, etc..
    $scope.getProjects = function getProjects() {
        var filter = '';
        if ($scope.projectFilter != null) {
            filter = '?description=' + $scope.projectFilter;
        }
        $http({
            method: "get",
            url: "api/v1/project" + filter,
        }).then(
            function success(response) {
                // @TODO: Of course, we actually want instances of project objects.
                $scope.projects = response.data.projects;
            },
            function failure(response) {
                console.log('Failure response: ', response);
            }
        );
    };
    $scope.getProjects();
    
});


ticktack.controller('TicketViewCtrl', ['$scope', '$http', '$stateParams', '$location', '$timeout', '$injector', 'referenceData', 'allProjects', 'allUsers', function ($scope, $http, $stateParams, $location, $timeout, $injector, referenceData, allProjects, allUsers) {
    
    // Make sure we have all the projects and all the users.
    allProjects.refresh();
    allUsers.refresh();
    
    // This controller is used for viewing a ticket. It is now also used for creating a new ticket
    $scope.ticket_id = $stateParams.hasOwnProperty('id') ? $stateParams.id : null;
    $scope.ticket = {};
    $scope.editingTicket = false;
    
    $scope.adding = {
        assignment: false,
        relationship: false,
        comment: false
    };
    // A spinner is shown when any of these values is greater than 1.
    $scope.spinners = {
        ticket: 0,
        assignment: 0,
        relationship: 0,
        comment: 0
    };
    
    $scope.staticData = {
        types: referenceData.getCodes('ticket_types'),
        priorities: referenceData.getCodes('ticket_priorities'),
        statuses: referenceData.getCodes('ticket_statuses')
    }
    
    $scope.projectData = {};
    $scope.userData = {};
    
    // @TODO: These codes need to come from the database.
    $scope.relationshipTypes = [
        {code: "associated", description: "associated with"},
        {code: "dependent", description: "a dependency of"},
        {code: "dependency", description: "dependent upon"},
        {code: "parent", description: "a child of"},
        {code: "child", description: "a parent of"}
    ];
    $scope.newRelationship = {
        relType: null,
        trgTicket: null
    };
    // Model for a new comment
    $scope.newComment = {
        detail: null
    }
    
    /**
     * Method to load the ticket and associated data, or just one piece of associated data,
     * into the controller.
     * @param string whichPart Which part of the data to load; pass null to load everything.
     */
    $scope.reloadTicketData = function reloadTicketData(whichPart) {
        // If we have a ticket, load its data.
        if ($scope.ticket_id != null) {
            // Call the API endpoint to get all the data.
            $http({
                method: "get",
                url: "api/v1/ticket/" + $scope.ticket_id,
            }).then(
                function success(response) {
                    // Now set the model appropriate to which part we are loading.
                    // If the data has not been loaded at all, override the parameter and assume "all".
                    if ($scope.ticket == null) {
                        $scope.ticket = response.data.tickets[0];
                        $scope.convertDate();
                    } else {
                        switch (whichPart) {
                            case 'assignments':
                                $scope.ticket.assignments = response.data.tickets[0].assignments;
                                break;
                            case 'associated':
                                $scope.ticket.associated = response.data.tickets[0].associated;
                                break;
                            case 'comments':
                                $scope.ticket.comments = response.data.tickets[0].comments;
                                break;
                            default:
                                $scope.ticket = response.data.tickets[0];
                                $scope.convertDate();
                                break;
                        }
                    }
                    // We seem to need to do this because the checkbox requires a true/false value.
                    for (var aidx = 0 ; aidx < $scope.ticket.associated.length ; aidx++ ) {
                        $scope.ticket.associated[aidx].pinned = ($scope.ticket.associated[aidx].pinned == '1');
                    }
                },
                function failure(response) {
                    alert('Failed to load the ticket. Click OK to see the list of tickets');
                    $location.path('/tickets');
                }
            );
        }
    }
    
    // @TODO: This would go in the Ticket model constructor eventually.
    $scope.convertDate = function convertDate() {
        if ($scope.ticket.due_datetime == '') {
            $scope.ticket.due_datetime_for_view = '';
        } else {
            // This is a hack to make it ignore time zones
            $scope.ticket.due_datetime = $scope.ticket.due_datetime.substr(0, 19);
            
            var tmp = new Date($scope.ticket.due_datetime);
            $scope.ticket.due_datetime_for_view = tmp.toLocaleDateString() + ' ' + tmp.toLocaleTimeString();
            $scope.ticket.due_datetime_for_view = $scope.ticket.due_datetime_for_view.substr(0, $scope.ticket.due_datetime_for_view.length - 3); // Remove the seconds component of the time.
        }
    }
    
    // Method to load all the tickets in the system, that the logged-in user can see.
    // @TODO: This needs to be done as a service.
    $scope.loadAllTickets = function loadAllTickets() {
        $http({
            method: "get",
            url: "api/v1/ticket",
        }).then(
            function success(response) {
                if (response.data.success) {
                    $scope.allTickets = response.data.tickets;
                }
            },
            function failure(response) {
                alert('Failed to load all tickets.');
            }
        );
    }
    
    $scope.startWork = function startWork(whichSection) {
        if ($scope.spinners.hasOwnProperty(whichSection)) {
            $scope.spinners[whichSection]++;
        }
    }
    
    $scope.finishWork = function startWork(whichSection) {
        if ($scope.spinners.hasOwnProperty(whichSection)) {
            if ($scope.spinners[whichSection] > 0) {
                $scope.spinners[whichSection]--;
            }
        }
    }
    
    $scope.getAssigneeType = function getAssigneeType(thisAssignee) {
        return (thisAssignee.sort_order == 1 ? 'Primary' : 'Other');
    }
    
    $scope.toggle = function toggle(which) {
        $scope.adding[which] = !$scope.adding[which];
        if (which == 'assignment') {
            $scope.populateAssigneeList();
        }
    }
    
    $scope.populateAssigneeList = function populateAssigneeList() {
        $scope.assignees = allUsers.getAllUsersData();
        if ($scope.assignees == null) {
            $scope.assignees = [];
        }
        // For some reason, it remembers this dummy row, so we have to check it's not there before we add it.
        if ($scope.assignees.length == 0 || $scope.assignees[0].id != -1) {
            $scope.assignees.unshift({
                id: -1,
                username: 'Please select...'
            });
        }
    }
    
    $scope.populateAssigneeList();
    $scope.newAssignee = {id: $scope.assignees[0].id};
    
    // Assignments
    
    // User wants to create a new assignment.
    $scope.saveAssignment = function saveAssignment() {
        if ($scope.newAssignee.id > 0) {
            $scope.startWork('assignment');
            // Send the new assignment to the server.
            $http({
                method: "post",
                url: "api/v1/ticket/" + $scope.ticket_id + '/assignment/' + $scope.newAssignee.id,
            }).then(
                function success(response) {
                    if (response.data.success) {
                        // Assignment was successful. Load all the assignments back into memory.
                        $scope.reloadTicketData('assignments');
                        // Set the selection to the blank option and close the form.
                        $scope.newAssignee.id = $scope.assignees[0].id;
                        $scope.toggle('assignment');
                    }
                    $scope.finishWork('assignment');
                },
                function failure(response) {
                    alert('There was a problem saving this assignment: ' + response.data.message);
                    $scope.finishWork('assignment');
                }
            );
        }
    }
    
    $scope.unassign = function unassign(assignmentId) {
        var confirmDelete = confirm('Are you sure you want to remove this assignment?');
        if (!confirmDelete) {
            return;
        }
        $scope.startWork('assignment');
        $http({
            method: 'delete',
            url: 'api/v1/assignment/' + assignmentId
        }).then(
            function success(response) {
                if (response.data.success) {
                    // Delete was successul. Load all the assignments back into memory.
                    $scope.reloadTicketData('assignments');
                }
                $scope.finishWork('assignment');
            },
            function failure(response) {
                $scope.finishWork('assignment');
            }
        )
    }
    
    $scope.moveAssignment = function moveAssignment(assignmentId, dir) {
        if (dir == 'up' || dir == 'down') {
            $scope.startWork('assignment');
            $http({
                method: 'put',
                url: 'api/v1/assignment/' + assignmentId + '/' + dir
            }).then(
                function success(response) {
                    if (response.data.success) {
                        // Movement was successul. Load all the assignments back into memory.
                        $scope.reloadTicketData('assignments');
                    }
                    $scope.finishWork('assignment');
                },
                function failure(response) {
                    console.log('Failure. Response...', response);
                    $scope.finishWork('assignment');
                }
            )
        } else {
            alert('Unknown direction: ' + dir);
        }
    }
    
    // Relationships
    
    // Adding a new association.
    $scope.saveRelationship = function saveRelationship() {
        if ($scope.newRelationship.relType == null) {
            alert('You must choose a relationship type');
            return false;
        }
        if ($scope.newRelationship.trgTicket == null) {
            alert('You must choose a ticket');
            return false;
        }
        $scope.startWork('relationship');
        // Send the new relationship to the server.
        $http({
            method: "post",
            url: 'api/v1/ticket/' + $scope.ticket_id + '/associated',
            data: {
                relationship_type: $scope.newRelationship.relType.code,
                target_ticket_id: $scope.newRelationship.trgTicket.id,
                is_pinned: false
            }
        }).then(
            function success(response) {
                if (response.data.success) {
                    // Save was successful. Load all the relationships back into memory.
                    $scope.reloadTicketData('relationships');
                    // Set the selection to the blank options and close the form.
                    $scope.newRelationship.relType = null;
                    $scope.newRelationship.trgTicket = null;
                    $scope.toggle('relationship');
                }
                $scope.finishWork('relationship');
            },
            function failure(response) {
                alert('There was a problem saving this relationship: ' + response.data.message);
                $scope.finishWork('relationship');
            }
        );
    }
    
    // Toggle the pinned state of a relationship.
    $scope.togglePinned = function togglePinned(associationRow) {
        $scope.startWork('relationship');
        var pinAction = associationRow.pinned ? 'pin' : 'unpin';
        $http({
            method: 'put',
            url: 'api/v1/ticket/' + $scope.ticket.id + '/associated/' + associationRow.id + '/' + pinAction
        }).then(
            function success(response) {
                if (response.data.success) {
                    // Pin/unpin was successul. Load all the associations back into memory.
                    $scope.reloadTicketData('associated');
                }
                $scope.finishWork('relationship');
            },
            function failure(response) {
                $scope.finishWork('relationship');
            }
        )
    }
    
    // Move an association up/down.
    $scope.moveAssociated = function moveAssociated(associatedId, dir) {
        if (dir == 'up' || dir == 'down') {
            $scope.startWork('relationship');
            $http({
                method: 'put',
                url: 'api/v1/ticket/' + $scope.ticket.id + '/associated/' + associatedId + '/' + dir
            }).then(
                function success(response) {
                    if (response.data.success) {
                        // Movement was successul. Load all the associations back into memory.
                        $scope.reloadTicketData('associated');
                    }
                    $scope.finishWork('relationship');
                },
                function failure(response) {
                    $scope.finishWork('relationship');
                }
            )
        } else {
            alert('Unknown direction: ' + dir);
        }
    }
    
    // Comments
    
    // Method to save a comment.
    $scope.saveComment = function saveComment() {
        if ($scope.newComment.detail) {
            $scope.startWork('comment');
            // Send the new comment to the server.
            $http({
                method: "post",
                url: 'api/v1/ticket/' + $scope.ticket_id + '/comment',
                data: {
                    details: $scope.newComment.detail
                    // "Added by" will default to the logged-in user.
                }
            }).then(
                function success(response) {
                    if (response.data.success) {
                        // Save was successful. Load all the comments back into memory.
                        $scope.reloadTicketData('comments');
                        // Set the selection to the blank options and close the form.
                        $scope.newComment.detail = null;
                        $scope.toggle('comment');
                    }
                    $scope.finishWork('comment');
                },
                function failure(response) {
                    alert('There was a problem saving your comment: ' + response.data.message);
                    $scope.finishWork('comment');
                }
            );
        } else {
            alert('You must actually enter a comment!');
        }
    }
    
    // Ticket editing methods.
    $scope.toggleEdit = function toggleEdit() {
        $scope.editingTicket = !$scope.editingTicket;
        if ($scope.editingTicket) {
            // We're editing a ticket - make sure the project- and user lists are up to date.
            $scope.projectData = allProjects.getAllProjectsData();
            $scope.userData = allUsers.getAllUsersData();
        } else {
            if ($scope.ticket_id == null) {
                // They've canceled an insert, so jump back to the ticket list.
                $location.path('/tickets');
            }
        }
    }
    
    if ($scope.ticket_id == null) {
        $timeout(function() {
            $scope.projectData = allProjects.getAllProjectsData();
            $scope.projectData.unshift({
                id: -1,
                name: 'Please select...'
            });
            $scope.userData = allUsers.getAllUsersData();
            // If created by user is not set, find out who is currently logged in, and set it to that.
            if ($scope.ticket.created_by_user_id == null) {
                var userSession = $injector.get('User');
                var loggedInUsername = userSession.getUser().username;
                for( var userIdx = 0 ; userIdx < $scope.userData.length ; userIdx++ ) {
                    if ($scope.userData[userIdx].username == loggedInUsername) {
                        $scope.ticket.created_by_user_id = $scope.userData[userIdx].id;
                    }
                }
                // If there's still no selection, pick the first one!
                if ($scope.ticket.created_by_user_id == null) {
                    $scope.ticket.created_by_user_id = $scope.userData[0].id;
                }
            }
        }, 1000);
        $scope.ticket = {
            id: null,
            project_id: -1,
            ticket_type: 'new feature',
            summary: 'New ticket...',
            description: null,
            created_by_user_id: null, // Will be to logged-in user in due course.
            priority: 'medium',
            status: 'new',
            due_datetime: null,
            estimated_length_seconds: null
        };
        $scope.toggleEdit();
    }
    
    // Save the ticket.
    $scope.saveTicket = function saveTicket() {
        // @TODO: Do any validation here.
        // Save the ticket.
        $scope.startWork('ticket');
        var saveMethod = $scope.ticket_id == null ? 'post' : 'put';
        var saveUrl = 'api/v1/ticket' + ($scope.ticket_id == null ? '' : '/' + $scope.ticket.id);
        $http({
            method: saveMethod,
            url: saveUrl,
            data: {
                project_id: $scope.ticket.project_id,
                ticket_type: $scope.ticket.ticket_type,
                summary: $scope.ticket.summary,
                description: $scope.ticket.description,
                created_by_user_id: $scope.ticket.created_by_user_id,
                priority: $scope.ticket.priority,
                status: $scope.ticket.status,
                due_datetime: $scope.ticket.due_datetime_for_view,
                estimated_length_seconds: $scope.ticket.estimated_length_for_view
            }
        }).then(
            function success(response) {
                if (response.data.success) {
                    // Save ticket was successul. Load the ticket back into memory.
                    if ($scope.ticket_id == null) {
                        $location.path('tickets/' + response.data.id);
                    } else {
                        $scope.reloadTicketData('ticket');
                        $scope.toggleEdit();
                    }
                }
                $scope.finishWork('ticket');
            },
            function failure(response) {
                alert(response.data.message);
                $scope.finishWork('ticket');
            }
        )
    }
    
    // Load all the data for this ticket.
    $scope.reloadTicketData();
    
    // Load the basic info for all tickets.
    // @TODO: This needs to be a service that runs once at startup, and maybe periodically (or after tickets are edited).
    $scope.loadAllTickets();
    
    $scope.codeToDesc = function codeToDesc(refType, refValue) {
        return referenceData.getDesc(refType, refValue);
    }
    
}]);


ticktack.controller('TicketsCtrl', ['$scope', '$http', 'allProjects', 'allUsers', function ($scope, $http, allProjects, allUsers) {
    
    $scope.ticketFilter = null;
    $scope.tickets = [];
    
    // Make sure we have all the projects and users.
    allProjects.refresh();
    allUsers.refresh();
    
    // @TODO: I'm sure we can encapsulate this in a service, and just pass in
    // @TODO: the method, url and data, and pass stuff back, etc..
    $scope.getTickets = function getTickets() {
        var filter = '';
        if ($scope.ticketFilter != null) {
            filter = '?summary=' + $scope.ticketFilter;
        }
        $http({
            method: "get",
            url: "api/v1/ticket" + filter,
        }).then(
            function success(response) {
                // @TODO: Of course, we actually want instances of ticket objects.
                $scope.tickets = response.data.tickets;
            },
            function failure(response) {
                console.log('Failure response: ', response);
            }
        );
    };
    
    // TODO: Of course, this needs to be moved into the ticket model, when we create it.
    $scope.getPrimaryAssigneeName = function getPrimaryAssigneeName(thisTicket) {
        if (thisTicket.assignments.length == 0) {
            return '[none]';
        }
        return thisTicket.assignments[0].user_name;
    }
    
    $scope.getTickets();
    
}]);


ticktack.controller('TicktackCtrl', function ($scope, $rootScope) {
    
    $scope.isLoggedIn = function isLoggedIn() {
        return typeof $rootScope.currentUser !== 'undefined';
    }
    
});
