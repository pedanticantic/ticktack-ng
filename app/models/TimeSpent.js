ticktack.factory('timeSpentModel', [function ticketFactory() {
    
    var TimeSpent = function TimeSpent() {
        // Attributes.
        this.id = null;
        this.ticketId = null;
        this.userId = null;
        this.userName = null;
        this.startDatetime = null;
        this.duration = null;
        this.durationFormatted = null;
        this.notes = null;
    };
    
    TimeSpent.prototype = {
        
        getFieldMap: function getFieldMap() {
            return {
                id: 'id',
                ticketId: 'ticket_id',
                userId: 'user_id',
                userName: 'username',
                startDatetime: 'start_datetime',
                duration: 'duration',
                durationFormatted: 'duration_formatted',
                notes: 'notes'
            }
        }
        
    };
    
    return TimeSpent;
}]);
