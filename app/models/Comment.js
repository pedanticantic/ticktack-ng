ticktack.factory('commentModel', [function ticketFactory() {
    
    var Comment = function Comment() {
        // Attributes.
        this.id = null;
        this.ticketId = null;
        this.addedByUserId = null;
        this.details = null;
        this.detailsMarkdown = null;
        this.createdAt = null;
        this.addedByUserName = null;
    };
    
    Comment.prototype = {
        
        getFieldMap: function getFieldMap() {
            return {
                id: 'id',
                ticketId: 'ticket_id',
                addedByUserId: 'added_by_user_id',
                details: 'details',
                detailsMarkdown: 'details_markdown',
                addedByUserName: 'added_by_user_name',
                createdAt: 'created_at'
            }
        }
        
    };
    
    return Comment;
}]);
