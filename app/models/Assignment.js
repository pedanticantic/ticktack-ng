ticktack.factory('assignmentModel', [function assignmentFactory() {
    
    var Assignment = function Assignment() {
        // Attributes.
        this.id = null;
        this.ticketId = null;
        this.userId = null;
        this.userName = null;
        this.sortOrder = null;
    };
    
    Assignment.prototype = {
        
        getFieldMap: function getFieldMap() {
            return {
                id: 'id',
                ticketId: 'ticket_id',
                userId: 'user_id',
                userName: 'user_name',
                sortOrder: 'sort_order',
            }
        },
    
        getAssigneeType: function getAssigneeType() {
            return (this.sortOrder == 1 ? 'Primary' : 'Other');
        },
        
        // @TODO: I'm sure this can be made generic, rather than specific to each model.
        // See the equivalent method in Ticket.js
        matches: function matches(filter) {
            var assignmentMatches = true;
            for( var filterType in filter) {
                var doesMatch = false;
                if (this.hasOwnProperty(filterType)) {
                    var filterCriteria = filter[filterType];
                    if (Array.isArray(filterCriteria)) {
                        for(var criteriaIndex = 0 ; criteriaIndex < filterCriteria.length ; criteriaIndex++) {
                            doesMatch = doesMatch || this[filterType] == filterCriteria[criteriaIndex];
                        }
                    } else {
                        // We have a special case here - if the filter starts with "!", we negate the
                        // match result.
                        if (typeof filterCriteria == 'string' && filterCriteria.substr(0, 1) == '!') {
                            filterCriteria = filterCriteria.substr(1);
                            doesMatch = this[filterType] != filterCriteria;
                        } else {
                            doesMatch = this[filterType] == filterCriteria;
                        }
                    }
                }
                assignmentMatches = assignmentMatches && doesMatch;
            }
            return assignmentMatches;
        }
        
    };
    
    return Assignment;
}]);
