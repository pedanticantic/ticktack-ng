ticktack.factory('ticketModel', ['referenceData', 'projectModel', 'assignmentModel', 'relationshipModel', 'commentModel', 'timeSpentModel', function ticketFactory(referenceData, projectModel, assignmentModel, relationshipModel, commentModel, timeSpentModel) {
    
    var Ticket = function Ticket() {
        // Attributes.
        this.id = null;
        this.projectId = -1;
        this.ticketType = 'new feature';
        this.summary = null;
        this.description = null;
        this.descriptionMarkdown = null;
        this.createdByUserId = null;
        this.createdByUserName = null;
        this.priority = 'medium';
        this.status = 'new';
        this.dueDatetime = null;
        this.dueDatetimeForView = null;
        this.estimatedLengthSeconds = null;
        this.estimatedLengthForView = null;
        
        this.project = {};
        this.assignments = [];
        this.associated = [];
        this.comments = [];
        this.timesSpent = [];
        this.timeSpentTotals = {};
        
        this.refData = referenceData;
    };
    
    Ticket.prototype = {
        
        getFieldMap: function getFieldMap() {
            return {
                id: 'id',
                projectId: 'project_id',
                ticketType: 'ticket_type',
                summary: 'summary',
                description: 'description',
                descriptionMarkdown: 'description_markdown',
                createdByUserId: 'created_by_user_id',
                createdByUserName: 'created_by_name',
                priority: 'priority',
                status: 'status',
                dueDatetime: 'due_datetime',
                estimatedLengthSeconds: 'estimated_length_seconds',
                estimatedLengthForView: 'estimated_length_for_view',
                project: function(projectData) {
                    // We expect one project.
                    var objectTranslator = new ticktack.ObjectTranslator();
                    return objectTranslator.translate(projectData, new projectModel());
                },
                assignments: function(assignmentData) {
                    // We expect a list of assignments.
                    var objectTranslator = new ticktack.ObjectTranslator();
                    return objectTranslator.translateArray(assignmentData, function() { return new assignmentModel(); });
                },
                associated: function(relationshipData) {
                    // We expect a list of relationships.
                    var objectTranslator = new ticktack.ObjectTranslator();
                    return objectTranslator.translateArray(relationshipData, function() { return new relationshipModel(); });
                },
                comments: function(commentData) {
                    // We expect a list of assignments.
                    var objectTranslator = new ticktack.ObjectTranslator();
                    return objectTranslator.translateArray(commentData, function() { return new commentModel(); });
                },
                timesSpent: function(timesSpentData) {
                    // We expect a list of time spent records.
                    var objectTranslator = new ticktack.ObjectTranslator();
                    return objectTranslator.translateArray(timesSpentData, function() { return new timeSpentModel(); });
                },
                timeSpentTotals: function(timeSpentTotalsData) {
                    return timeSpentTotalsData;
                }
            }
        },
        
        triggerPostTranslate: function triggerPostTranslate() {
            if (this.dueDatetime == '') {
                this.dueDatetimeForView = '';
            } else {
                // This is a hack to make it ignore time zones
                this.dueDatetime = this.dueDatetime.substr(0, 19);
                
                var tmp = new Date(this.dueDatetime);
                this.dueDatetimeForView = tmp.toLocaleDateString() + ' ' + tmp.toLocaleTimeString();
                this.dueDatetimeForView = this.dueDatetimeForView.substr(0, this.dueDatetimeForView.length - 3); // Remove the seconds component of the time.
            }
        },
        
        setCreatedByUserId: function setCreatedByUserId(newUserId) {
            this.createdByUserId = newUserId;
        },
        
        getCreatedByUserId: function getCreatedByUserId() {
            return this.createdByUserId;
        },
    
        getPrimaryAssigneeName: function getPrimaryAssigneeName() {
            if (this.assignments.length == 0) {
                return '[none]';
            }
            return this.assignments[0].userName;
        },
        
        // Method to return true if the ticket matches the given criteria.
        // The filter is an object; each property is of the form:
        //   <ticketProperty>: <filterValue>
        // filterValue can be a scalar, or it can be an array.
        // If it's an array, the filter matches if any elements in the array match the ticket property value.
        // Method returns true if every property of the filter is a positive match.
        // @TODO: I'm sure this can be made generic, rather than specific to each model.
        matches: function matches(filter) {
            var ticketMatches = true;
            for( var filterType in filter) {
                var doesMatch = false;
                if (this.hasOwnProperty(filterType)) {
                    var filterCriteria = filter[filterType];
                    // Assignments is a special case. We need to loop through them and see if any match
                    // the criteria.
                    if (filterType == 'assignments') {
                        for(var assIndex = 0 ; assIndex < this.assignments.length ; assIndex++) {
                            doesMatch = doesMatch || this.assignments[assIndex].matches(filterCriteria);
                        }
                    } else {
                        if (Array.isArray(filterCriteria)) {
                            for(var criteriaIndex = 0 ; criteriaIndex < filterCriteria.length ; criteriaIndex++) {
                                doesMatch = doesMatch || this[filterType] == filterCriteria[criteriaIndex];
                            }
                        } else {
                            doesMatch = this[filterType] == filterCriteria;
                        }
                    }
                }
                ticketMatches = ticketMatches && doesMatch;
            }
            return ticketMatches;
        },
        
        // Methods to return the description for the given field and value.
        getTicketTypeDesc: function getTicketTypeDesc() {
            return this.refData.getDesc('ticket_types', this.ticketType);
        },
        getPriorityDesc: function getPriorityDesc() {
            return this.refData.getDesc('ticket_priorities', this.priority);
        },
        getStatusDesc: function getStatusDesc() {
            return this.refData.getDesc('ticket_statuses', this.status);
        }
        
    };
    
    return Ticket;
}]);
