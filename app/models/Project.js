ticktack.factory('projectModel', [function projectFactory() {
    
    var Project = function Project() {
        // Attributes.
        this.id = null;
        this.name = null;
        this.description = null;
        this.descriptionAsMarkdown = null;
        this.leadUserId = null;
        this.leadUserName = null;
    };
    
    Project.prototype = {
        
        getFieldMap: function getFieldMap() {
            return {
                id: 'id',
                name: 'name',
                description: 'description',
                descriptionAsMarkdown: 'description_markdown',
                leadUserId: 'lead_user_id',
                leadUserName: 'lead_user_username'
            }
        }
        
    };
    
    return Project;
}]);
