ticktack.factory('relationshipModel', [function relationshipFactory() {
    
    var Relationship = function Relationship() {
        // Attributes.
        this.id = null;
        this.relationshipType = null;
        this.srcTicketId = null;
        this.trgSummary = null;
        this.srcSortOrder = null;
        this.trgTicketId = null;
        this.trgSortOrder = null;
        this.pinned = null;

    };
    
    Relationship.prototype = {
        
        getFieldMap: function getFieldMap() {
            return {
                id: 'id',
                relationshipType: 'relationship_type',
                srcTicketId: 'src_ticket_id',
                trgSummary: 'trg_summary',
                srcSortOrder: 'src_sort_order',
                trgTicketId: 'trg_ticket_id',
                trgSortOrder: 'trg_sort_order',
                pinned: 'pinned'
            }
        }
        
    };
    
    return Relationship;
}]);
