ticktack.factory('userModel', [function userFactory() {
    
    var User = function User() {
        // Attributes.
        this.id = null;
        this.userName = null;
    };
    
    User.prototype = {
        
        getFieldMap: function getFieldMap() {
            return {
                id: 'id',
                userName: 'username'
            }
        }
        
    };
    
    return User;
}]);
