'use strict';

// Declare app level module which depends on views, and components
var ticktack = angular.module('ticktack', [
    'ui.router',
    'ui.bootstrap',
    'ngResource',
    'templates-main'
]);

/* This is to do with intercepting a 401 coming from the server - it means the
   user was logged in, but isn't now.
ticktack.config(function ($httpProvider) {

    $httpProvider.interceptors.push(function ($timeout, $q, $injector) {
        var loginModal, $http, $state;

        // this trick must be done so that we don't receive
        // `Uncaught Error: [$injector:cdep] Circular dependency found`
        $timeout(function () {
            loginModal = $injector.get('loginModal');
            $http = $injector.get('$http');
            $state = $injector.get('$state');
        });

        return {
            responseError: function (rejection) {
                if (rejection.status !== 401) {
                    return rejection;
                }

                var deferred = $q.defer();

                loginModal()
                    .then(function () {
                        deferred.resolve( $http(rejection.config) );
                    })
                    .catch(function () {
                        $state.go('welcome');
                        deferred.reject(rejection);
                    });

                return deferred.promise;
            }
        };
    });

});
*/

// For API calls, we need to convert the app host to the server host and add
// the auth headers.
ticktack.config(function ($httpProvider) {
    $httpProvider.interceptors.push(function ($q, $location, $rootScope) {
        return {
            'request': function (config) {
                if (config.url.substr(0, 4) == 'api/') {
                    var serverHost = $location.$$host;
                    switch (serverHost) {
                        case 'ticktack.ng.dev':
                            serverHost = 'http://ticktack.dev:8088/';
                            break;
                        case 'ticktack-ng.pedanticantic.click':
                            serverHost = 'http://ticktack.pedanticantic.click/';
                            break;
                    }
                    config.url = serverHost + config.url;
                    // Sort out the user credentials. Only if it's a call to the API
                    // and it's not a login request, and we have a valid user.
                    if ($rootScope.currentUser && config.url.indexOf('/login') == -1) {
                        config.headers.Authorization = $rootScope.currentUser.getAuthorization();
                    }
                }
                return config || $q.when(config);
            }
        }
    });
});

ticktack.filter('sanitise', ['$sce', function($sce) {
    return function(htmlCode){
        return $sce.trustAsHtml(htmlCode);
    }
}]);

ticktack.filter('excludeItself', function() {
    return function(inputArray, ticketId) {
        return inputArray.filter(function(item) {
            // If the array element's ticket id matches the one passed in, exclude it from the result.
            return !ticketId || !angular.equals(item.id, ticketId);
        });
    };
});

ticktack.run(function ($rootScope, $state, $injector, $http, loginModal, UsersApi, referenceData) {

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
        var userSession = $injector.get('LoggedInUser');
        var userCredentials = userSession.getUser();
        if (toState.data.forgetLogin === true) {
            $rootScope.currentUser = undefined;
            userSession.setUser(undefined, undefined);
        } else {
            var requireLogin = toState.data.requireLogin;

            if (requireLogin && typeof $rootScope.currentUser === 'undefined') {
                event.preventDefault();
                
                // User isn't logged in. See if we still have their credentials. If so
                // auto-log them in. This happens if user refreshes the screen.
                if (userCredentials &&
                    userCredentials.username != '' &&
                    userCredentials.username != 'undefined' &&
                    userCredentials.username != undefined &&
                    userCredentials.password != '' &&
                    userCredentials.password != 'undefined' &&
                    userCredentials.password != undefined) {
                    UsersApi.login(userCredentials.username, userCredentials.password)
                        .then(function (user) {
                            $rootScope.currentUser = user;
                            $state.go(toState.name, toParams)
                        }, function() {
                            console.log('Something went wrong with auto login', userCredentials);
                        });
                } else {
                    
                    // User is not logged in and must have explicitly logged out. Show the login form.
                    loginModal()
                        .then(function () {
                            return $state.go(toState.name, toParams);
                        })
                        .catch(function () {
                            // User canceled the login modal. Go to the default page.
                            return $state.go('home');
                        });
                }
            }
        }
    });
    
    // Call the API endpoint to get all the static reference data.
    $http({
        method: "get",
        url: "api/v1/referenceData",
    }).then(
        function success(response) {
            if (response.data.success) {
                referenceData.setReferenceData(response.data.referenceData);
            }
        },
        function failure(response) {
            alert('Failed to load the reference data.');
            $location.path('/home');
        }
    );

});
