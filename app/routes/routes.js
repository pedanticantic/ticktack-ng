ticktack.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');

    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'partials/home.html',
            // ...
            data: {
                requireLogin: false
            }
        })
        
        .state('register', {
            url: '/register',
            templateUrl: 'partials/register.html',
            controller: 'RegisterCtrl',
            // ...
            data: {
                requireLogin: false
            }
        })
        
        .state('version-history', {
            url: '/version-history',
            templateUrl: 'partials/version-history.html',
            // ...
            data: {
                requireLogin: false
            }
        })
        
        // Projects
        .state('projects', {
            url: '/projects',
            templateUrl: 'partials/projects.html',
            controller: 'ProjectsCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        .state('projectcreate', {
            url: '/projects/create',
            templateUrl: 'partials/project-edit.html',
            controller: 'ProjectEditCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        .state('projectview', {
            url: '/projects/:id',
            templateUrl: 'partials/project-view.html',
            controller: 'ProjectViewCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        .state('projectedit', {
            url: '/projects/:id/edit',
            templateUrl: 'partials/project-edit.html',
            controller: 'ProjectEditCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        
        // Tickets
        // They have a _lot_ of filters!
        .state('tickets', {
            url: '/tickets?summary&description&projectId&ticketTypes&createdByUserId&statuses&priorities&assignmentUserIds&assignmentLevel',
            templateUrl: 'partials/tickets.html',
            controller: 'TicketsCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        .state('ticketview', {
            url: '/tickets/:id',
            templateUrl: 'partials/ticket-view.html',
            controller: 'TicketViewCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        .state('ticketadd', {
            url: '/ticket',
            templateUrl: 'partials/ticket-view.html',
            controller: 'TicketViewCtrl',
            // ...
            data: {
                requireLogin: true
            }
        })
        
        // Logout
        .state('logout', {
            url: '/logout',
            templateUrl: 'partials/logout.html',
            // ...
            data: {
                forgetLogin: true
            }
        })
        .state('app', {
            abstract: true,
            // ...
            data: {
                requireLogin: true // this property will apply to all children of 'app'
            }
        })

});