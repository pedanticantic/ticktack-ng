ticktack.factory('referenceData', function ($rootScope) {
    
    var referenceData = null;
    
    // Method to set the entire collection of reference data.
    var _set = function _set(refData) {
        this.referenceData = refData;
    }
    
    // Method to return all the reference data
    var _get = function _get() {
        return this.referenceData;
    }
    
    // Method to return all the codes/descriptions for one type.
    var _getCodes = function _getCodes(refType) {
        if (this.referenceData.hasOwnProperty(refType)) {
            return this.referenceData[refType];
        }
        return false;
    }
    
    // Method to return the description for the given type and index.
    var _getDesc = function _getDesc(refType, refCode) {
        if (this.referenceData.hasOwnProperty(refType)) {
            for( var rdIdx = 0 ; rdIdx < this.referenceData[refType].length ; rdIdx++ ) {
                if (this.referenceData[refType][rdIdx].code == refCode) {
                    return this.referenceData[refType][rdIdx].desc;
                }
            }
        }
        return false;
    }
    
    return {
        setReferenceData: _set,
        getReferenceData: _get,
        getCodes: _getCodes,
        getDesc:_getDesc
    };
    
});