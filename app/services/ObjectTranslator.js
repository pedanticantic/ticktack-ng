ticktack.ObjectTranslator = (function () {

    var ObjectTranslator = function ObjectTranslator() {
    };
    
    /**
     * Method to take an array of data rows, and translate each one into an object.
     * @param array data The array of rows of records.
     * @param object modelInstance An (empty) instance of the model.
     *
     * @return object[] An array of models.
     */
    function translate(data, modelInstance) {
        // Check that the model has a field map method.
        if (modelInstance.getFieldMap && typeof modelInstance.getFieldMap == 'function') {
            var fieldMap = modelInstance.getFieldMap();
            // Loop through the fields, assigning the object property to the dataset value.
            for (var modelField in fieldMap) {
                if (modelInstance.hasOwnProperty(modelField)) {
                    if (typeof fieldMap[modelField] == 'function') {
                        modelInstance[modelField] = fieldMap[modelField](data[modelField]);
                    } else {
                        modelInstance[modelField] = data[fieldMap[modelField]];
                    }
                }
            }
            // See if the model has a "post-translate" trigger. If so, run it.
            if (modelInstance.triggerPostTranslate && typeof modelInstance.triggerPostTranslate == 'function') {
                modelInstance.triggerPostTranslate();
            }
        }
        // Return the array of instances of the model.
        return modelInstance;
    }
    
    /**
     * Method to take an array of data rows, and translate each one into an object.
     * @param array dataSet The array of rows of records.
     * @param function modelInstanceCallback Callback method to return an instance of a model.
     *
     * @return object[] An array of models.
     */
    function translateArray(dataSet, modelInstanceCallback) {
        var modelSet = [];
        for (var rowIndex in dataSet) {
            // Translate this row into a model and add it to the list.
            modelSet.push(translate(dataSet[rowIndex], modelInstanceCallback()));
        }
        return modelSet;
    }
    
    ObjectTranslator.prototype = {
        translate: translate,
        translateArray: translateArray
    }
    
    return ObjectTranslator;
}());