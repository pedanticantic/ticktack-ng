ticktack.service('LoggedInUser', function () {
    
    var retrieveUser = function retrieveUser() {
        if (localStorage.username && localStorage.password) {
            user.username = localStorage.username;
            user.password = localStorage.password;
        }
    }
    
    var saveUser = function saveUser() {
        localStorage.username = user.username;
        localStorage.password = user.password;
    }
    
    /**
     * @var Underlying object holding the user details.
     */
    var user = {
        username: '',
        password: ''
    }
    retrieveUser();
    
    /**
     * Method to set the username and password in the internal variable.
     * @param username
     * @param password
     */
    function setUser(username, password) {
        user.username = username;
        user.password = password;
        saveUser();
    }
    
    /**
     * Method to return the internal variable.
     * @return user
     */
    function getUser() {
        var un = user.username;
        var pw = user.password;
        return {username: un, password: pw};
    }
    
    /**
     * Method to return the id from the given list of users, where the user name
     * matches the currently logged in user.
     */
    function getLoggedInUserId(userData) {
        if (userData == null) {
            return null;
        }
        for(var userIdx = 0 ; userIdx < userData.length ; userIdx++) {
            if (userData[userIdx].userName == user.username) {
                return userData[userIdx].id;
            }
        }
        return null;
    }
    
    /**
     * Method to return the authorization string for this user.
     * @return string
     */
    function getAuthorization() {
        return 'Basic ' + window.btoa(user.username + ':' + user.password);
    }

    return {
        setUser: setUser,
        getUser: getUser,
        getAuthorization: getAuthorization,
        getLoggedInUserId: getLoggedInUserId
    };

});