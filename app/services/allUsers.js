ticktack.factory('allUsers', function ($rootScope, $http, userModel) {
    
    var userList = null;
    
    // Method to call the API endpoint to get all the users.
    var _refresh = function _refresh() {
        $http({
            method: "get",
            url: "api/v1/user",
        }).then(
            function success(response) {
                if (response.data.success) {
                    var objectTranslator = new ticktack.ObjectTranslator();
                    userList = objectTranslator.translateArray(response.data.users, function() { return new userModel(); });
                }
            },
            function failure(response) {
                alert('Failed to load the users.');
            }
        );
    }
    
    // Method to return all the users.
    var _get = function _get() {
        return userList;
    }
    
    return {
        refresh: _refresh,
        getAllUsersData: _get
    };
    
});