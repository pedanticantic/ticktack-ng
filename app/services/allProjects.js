ticktack.factory('allProjects', function ($rootScope, $http, projectModel) {
    
    var projectList = null;
    
    // Method to call the API endpoint to get all the projects.
    var _refresh = function _refresh() {
        $http({
            method: "get",
            url: "api/v1/project",
        }).then(
            function success(response) {
                if (response.data.success) {
                    var objectTranslator = new ticktack.ObjectTranslator();
                    projectList = objectTranslator.translateArray(response.data.projects, function() { return new projectModel(); });
                }
            },
            function failure(response) {
                alert('Failed to load the projects.');
            }
        );
    }
    
    // Method to return all the projects.
    var _get = function _get() {
        return projectList;
    }
    
    return {
        refresh: _refresh,
        getAllProjectsData: _get
    };
    
});