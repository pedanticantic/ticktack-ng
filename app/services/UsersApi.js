ticktack.factory("UsersApi", function ($q, $http, $injector) {
    // @TODO: This is a very crude login mechanism. We need to use OAUTH and tokens, etc.
    function _login(username, password) {
        var d = $q.defer();
        
        setTimeout(function () {
            $http({
                method: 'get',
                url: 'api/v1/login',
                params: {
                    username: username,
                    password: password
                }
            }).then(
                function success(response) {
                    // Response came back okay. See if it says the credentials were valid.
                    if (response.data.success) {
                        var userSession = $injector.get('LoggedInUser');
                        userSession.setUser(username, password);
                        d.resolve(userSession);
                    } else {
console.log('Login was rejected');
                        d.reject();
                    }
                },
                function failure(response) {
console.log('Login failed');
                    // Response failed for some reason. Assume the credentials are not valid.
                    // @TODO: We should tell the user and handle it better.
                    d.reject();
                }
            );
        }, 100);
        return d.promise
    }
    return { login: _login };
});