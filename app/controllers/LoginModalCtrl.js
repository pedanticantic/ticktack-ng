ticktack.controller('LoginModalCtrl', function ($scope, $timeout, $injector, UsersApi) {

    var userSession = $injector.get('LoggedInUser');
    var userCredentials = userSession.getUser();
    $scope.loginUsername = userCredentials.username;
    $scope.loginPassword = userCredentials.password;
    
    this.loginIsValid = true;
    
    this.cancel = $scope.$dismiss;

    this.submit = function (username, password) {
        var self = this;
        UsersApi.login(username, password)
            .then(function (user) {
                // Login was valid.
                this.loginIsValid = true;
                $scope.$close(user);
            }, function() {
                // Login wasn't valid.
                self.loginIsValid = false;
                $timeout(function() {
                    self.loginIsValid = true;
                }, 2000);
            });
    };

});