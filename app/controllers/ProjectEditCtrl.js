ticktack.controller('ProjectEditCtrl', ['$scope', '$http', '$stateParams', '$location', 'allProjects', 'projectModel', function ($scope, $http, $stateParams, $location, allProjects, projectModel) {
    
    // This controller is used for both creating and editing projects.
    $scope.project = {};
    
    $scope.projectIsLoaded = false;
    
    // Determine whether the user is creating a new project or editing one.
    $scope.mode = ('id' in $stateParams) ? 'update' : 'insert';
    
    // Load the project into the client.
    if ($scope.mode == 'update') {
        $http({
            method: "get",
            url: "api/v1/project/" + $stateParams.id,
        }).then(
            function success(response) {
                var objectTranslator = new ticktack.ObjectTranslator();
                $scope.project = objectTranslator.translate(response.data.projects[0], new projectModel());
                $scope.projectIsLoaded = true;  // User can save it if necessary.
            },
            function failure(response) {
                alert('Failed to load the project. Click OK to see the list of projects');
                $location.path('/projects');
            }
        );
    }
    
    $scope.saveProject = function saveProject() {
        if ($scope.projectIsLoaded || $scope.mode == 'insert') {
            if ($scope.editProject.$valid) {
                // Save the project.
                var saveVerb = ($scope.mode == 'update' ? 'put' : 'post');
                var saveUrl = "api/v1/project" + ($scope.mode == 'update' ? ('/' + $stateParams.id) : '');
                $http({
                    method: saveVerb,
                    url: saveUrl,
                    data: {
                        'name': $scope.project.name,
                        'description': $scope.project.description
                    }
                }).then(
                    function success(response) {
                        alert('Project was saved');
                        allProjects.refresh(); // Refresh the list of projects for the droplist in the ticket.
                        $location.path('/projects');
                    },
                    function failure(response) {
                        var message = 'Unknown error';
                        if (response.data) {
                            if (response.data.message) {
                                message = response.data.message;
                            }
                        }
                        alert('Save failed. Response: ' + message);
                        console.log('Failure response: ', response); // Full details
                    }
                );
            } else {
                alert('There are errors in the form (eg. min/max field length)');
            }
        } else {
            alert('Please wait for the project to load...');
        }
    }
    
    $scope.getModeDesc = function getModeDesc() {
        return $scope.mode == 'insert' ? 'New' : 'Edit';
    }
    
}]);
