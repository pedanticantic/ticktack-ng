ticktack.controller('TicketsCtrl', ['$scope', '$http', '$stateParams', '$location', 'allProjects', 'allUsers', 'referenceData', 'ticketModel', function ($scope, $http, $stateParams, $location, allProjects, allUsers, referenceData, ticketModel) {
    
    $scope.tickets = [];
    $scope.projectData = [];
    $scope.userData = [];
    $scope.filter = {};
    $scope.filterFirstTime = true;
    $scope.loading = true;
    
    $scope.isLoading = function isLoading() {
        return $scope.loading;
    }
    
    $scope.staticData = {
        types: referenceData.getCodes('ticket_types'),
        priorities: referenceData.getCodes('ticket_priorities'),
        statuses: referenceData.getCodes('ticket_statuses')
    }
    
    $scope.filterIsVisible = false; // Hide the ticket filter by default.
    
    // Make sure we have all the projects and users.
    allProjects.refresh();
    allUsers.refresh();
    
    // @TODO: I'm sure we can encapsulate this in a service, and just pass in
    // @TODO: the method, url and data, and pass stuff back, etc..
    $scope.getTickets = function getTickets() {
        $scope.loading = true;
        // Note: statuses, priorities and assignment user ids can be arrays, and they are implicitly
        // converted to a CSL here.
        var filter = [];
        if ($stateParams.summary != null) {
            filter.push('summary=' + $stateParams.summary);
        }
        if ($stateParams.description != null) {
            filter.push('description=' + $stateParams.description);
        }
        if ($stateParams.projectId != null) {
            filter.push('project_id=' + $stateParams.projectId);
        }
        if ($stateParams.ticketTypes != null) {
            filter.push('ticket_types=' + $stateParams.ticketTypes);
        }
        if ($stateParams.createdByUserId != null) {
            filter.push('created_by_user_id=' + $stateParams.createdByUserId);
        }
        if ($stateParams.statuses != null) {
            filter.push('statuses=' + $stateParams.statuses);
        }
        if ($stateParams.priorities != null) {
            filter.push('priorities=' + $stateParams.priorities);
        }
        if ($stateParams.assignmentUserIds != null) {
            filter.push('assignment_user_ids=' + $stateParams.assignmentUserIds);
        }
        if ($stateParams.assignmentLevel != null) {
            filter.push('assignment_level=' + $stateParams.assignmentLevel);
        }
        $http({
            method: "get",
            url: "api/v1/ticket" + (filter.length > 0 ? '?' + filter.join('&') : ''),
        }).then(
            function success(response) {
                $scope.loading = false;
                var objectTranslator = new ticktack.ObjectTranslator();
                $scope.tickets = objectTranslator.translateArray(response.data.tickets, function() { return new ticketModel(); });
            },
            function failure(response) {
                $scope.loading = false;
                console.log('Failure response: ', response);
            }
        );
    };
    
    $scope.clearFilter = function clearFilter(optionalEvent) {
        if (optionalEvent != undefined) {
            optionalEvent.preventDefault();
        }
        $scope.filter = {
            summary: null,
            description: null,
            projectId: null,
            type: {},
            createdByUserId: null,
            priority: {},
            status: {},
            assignment: {
                userId: null,
                level: ''
            }
        };
        for(var typeIndex = 0 ; typeIndex < $scope.staticData.types.length ; typeIndex++) {
            $scope.filter.type[$scope.staticData.types[typeIndex].code] = false;
        }
        for(var priorityIndex = 0 ; priorityIndex < $scope.staticData.priorities.length ; priorityIndex++) {
            $scope.filter.priority[$scope.staticData.priorities[priorityIndex].code] = false;
        }
        for(var statusIndex = 0 ; statusIndex < $scope.staticData.priorities.length ; statusIndex++) {
            $scope.filter.status[$scope.staticData.statuses[statusIndex].code] = false;
        }
    }
    
    $scope.populateFilter = function populateFilter() {
        if ($stateParams.summary != null) {
            $scope.filter.summary = $stateParams.summary;
        }
        if ($stateParams.description != null) {
            $scope.filter.description = $stateParams.description;
        }
        if ($stateParams.projectId != null) {
            $scope.filter.projectId = $stateParams.projectId;
        }
        if ($stateParams.ticketTypes != null) {
            $scope.setFilterOptionsToTrue('type', $stateParams.ticketTypes);
        }
        if ($stateParams.createdByUserId != null) {
            $scope.filter.createdByUserId = $stateParams.createdByUserId;
        }
        if ($stateParams.statuses != null) {
            $scope.setFilterOptionsToTrue('status', $stateParams.statuses);
        }
        if ($stateParams.priorities != null) {
            $scope.setFilterOptionsToTrue('priority', $stateParams.priorities);
        }
        if ($stateParams.assignmentUserIds != null) {
            $scope.filter.assignment.userId = $stateParams.assignmentUserIds;
        }
        if ($stateParams.assignmentLevel != null) {
            $scope.filter.assignment.level = $stateParams.assignmentLevel;
        }
    }
    
    $scope.toggleShowFilter = function toggleShowFilter() {
        $scope.filterIsVisible = !$scope.filterIsVisible;
        if ($scope.filterIsVisible) {
            // We're in the filter view - make sure the project- and user lists are up to date.
            if ($scope.filterFirstTime) {
                $scope.filterFirstTime = false;
                $scope.projectData = allProjects.getAllProjectsData();
                $scope.userData = allUsers.getAllUsersData();
            }
        }
    }
    
    $scope.applyFilter = function applyFilter() {
        // Build up the search options from the filter options.
        var options = {};
        if ($scope.filter.summary) {
            options.summary = $scope.filter.summary;
        }
        if ($scope.filter.description) {
            options.description = $scope.filter.description;
        }
        if ($scope.filter.projectId) {
            options.projectId = $scope.filter.projectId;
        }
        var typeOptions = $scope.getTrueFilterOptions($scope.filter.type);
        if (typeOptions != '') {
            options.ticketTypes = typeOptions;
        }
        if ($scope.filter.createdByUserId) {
            options.createdByUserId = $scope.filter.createdByUserId;
        }
        var priorityOptions = $scope.getTrueFilterOptions($scope.filter.priority);
        if (priorityOptions != '') {
            options.priorities = priorityOptions;
        }
        var statusOptions = $scope.getTrueFilterOptions($scope.filter.status);
        if (statusOptions != '') {
            options.statuses = statusOptions;
        }
        if ($scope.filter.assignment.userId) {
            options.assignmentUserIds = $scope.filter.assignment.userId;
            options.assignmentLevel = $scope.filter.assignment.level;
        }
        // And load the page.
        $location.path('/tickets').search(options);
    }
    
    /**
     * Method to return the key name of any property in the given object that has a value of true.
     */
    $scope.getTrueFilterOptions = function getTrueFilterOptions(filterObject) {
        var result = [];
        for(var filterProp in filterObject) {
            if (filterObject.hasOwnProperty(filterProp) && filterObject[filterProp] === true) {
                result.push(filterProp);
            }
        }
        return result.join(',');
    }
    
    /**
     * Method to take all the values in a CSL and set the property with the same name in the
     * given property of the filter object to true.
     * It seems that a newer version of Angular causes the CSL values to be passed in as an array
     * in some instances, so we now have to check for that.
     */
    $scope.setFilterOptionsToTrue = function setFilterOptionsToTrue(filterProperty, propertyCSL) {
        var propertyList = propertyCSL;
        if (!Array.isArray(propertyList)) {
            propertyList = propertyList.split(',');
        }
        if (propertyList.length > 0) {
            for(var propIndex = 0 ; propIndex < propertyList.length ; propIndex++) {
                $scope.filter[filterProperty][propertyList[propIndex]] = true;
            }
        }
    }
    
    $scope.getTickets();
    
    $scope.clearFilter();
    $scope.populateFilter();
    
}]);
