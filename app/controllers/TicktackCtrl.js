ticktack.controller('TicktackCtrl', function ($scope, $rootScope) {
    
    $scope.isLoggedIn = function isLoggedIn() {
        return typeof $rootScope.currentUser !== 'undefined';
    }
    
    $scope.loggedInUser = function loggedInUser() {
        if ($scope.isLoggedIn()) {
            return $rootScope.currentUser.getUser().username;
        }
        return null;
    }
    
});
