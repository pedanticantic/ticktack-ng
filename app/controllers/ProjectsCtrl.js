ticktack.controller('ProjectsCtrl', function ($scope, $http, projectModel) {
    
    $scope.projectFilter = null;
    $scope.projects = [];
    $scope.loading = true;
    
    $scope.isLoading = function isLoading() {
        return $scope.loading;
    }
    
    // @TODO: I'm sure we can encapsulate this in a service, and just pass in
    // @TODO: the method, url and data, and pass stuff back, etc..
    // @TODO: Do this for all $http interactions.
    $scope.getProjects = function getProjects() {
        $scope.loading = true;
        var filter = '';
        if ($scope.projectFilter != null) {
            filter = '?description=' + $scope.projectFilter;
        }
        $http({
            method: "get",
            url: "api/v1/project" + filter,
        }).then(
            function success(response) {
                $scope.loading = false;
                var objectTranslator = new ticktack.ObjectTranslator();
                $scope.projects = objectTranslator.translateArray(response.data.projects, function() { return new projectModel(); });
            },
            function failure(response) {
                $scope.loading = false;
                console.log('Failure response: ', response);
            }
        );
    };
    $scope.getProjects();
    
});
