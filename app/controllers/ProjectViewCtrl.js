ticktack.controller('ProjectViewCtrl', ['$scope', '$http', '$stateParams', '$injector', 'referenceData', 'allUsers', 'ticketModel', 'projectModel', function ($scope, $http, $stateParams, $injector, referenceData, allUsers, ticketModel, projectModel) {
    
    // This controller is used for viewing a project.
    $scope.projectId = $stateParams.id;
    $scope.project = {};
    $scope.projectTickets = null;
    $scope.ticketStatuses = referenceData.getCodes('ticket_statuses');
    $scope.ticketPriorities = referenceData.getCodes('ticket_priorities'),
    
    allUsers.refresh();
    
    // Load the project into the client.
    $http({
        method: "get",
        url: "api/v1/project/" + $scope.projectId,
    }).then(
        function success(response) {
            var objectTranslator = new ticktack.ObjectTranslator();
            $scope.project = objectTranslator.translate(response.data.projects[0], new projectModel());
        },
        function failure(response) {
            alert('Failed to load the project. Click OK to see the list of projects');
            $location.path('/projects');
        }
    );
    
    // @TODO: I'm sure we can encapsulate this in a service, and just pass in
    // @TODO: the method, url and data, and pass stuff back, etc..
    $scope.getProjectTickets = function getProjectTickets() {
        $http({
            method: "get",
            url: "api/v1/project/" + $scope.projectId + "/ticket",
        }).then(
            function success(response) {
                var objectTranslator = new ticktack.ObjectTranslator();
                $scope.projectTickets = objectTranslator.translateArray(response.data.tickets, function() { return new ticketModel(); });
            },
            function failure(response) {
                console.log('Failure response: ', response);
            }
        );
    };
    
    $scope.getProjectTickets();
    
    $scope.getLoggedInUserId = function getLoggedInUserId() {
        var userSession = $injector.get('LoggedInUser');
        return userSession.getLoggedInUserId(allUsers.getAllUsersData());
    }
    
    // Method to return a list of tickets that match the filter.
    // Note: the ticket list is already filtered by tickets in this project.
    $scope.ticketFilter = function ticketFilter(filter) {
        // If we haven't loaded the tickets for this project yet, just return nothing.
        if ($scope.projectTickets == null) {
            return null;
        }
        var matches = [];
        for( var ticketIndex = 0 ; ticketIndex < $scope.projectTickets.length ; ticketIndex++ ) {
            if ($scope.projectTickets[ticketIndex].matches(filter)) {
                matches.push($scope.projectTickets[ticketIndex]);
            }
        }
        return matches;
    }
    
    $scope.countTicketFilter = function countTicketFilter(filter) {
        // If we haven't loaded the tickets for this project yet, just return nothing.
        if ($scope.projectTickets == null) {
            return null;
        }
        // Okay, the project tickets are available. Find all that match the filter, and return the count
        // of the result.
        var matchingTickets = $scope.ticketFilter(filter);
        return matchingTickets.length;
    }
    
}]);
