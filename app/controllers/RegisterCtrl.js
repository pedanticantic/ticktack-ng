ticktack.controller('RegisterCtrl', function ($scope, $http, $location, UsersApi) {
    
    $scope.username = null;
    $scope.password = null;
    $scope.confirm_password = null;
    
    $scope.errorMessage = [];
    
    // @TODO: I'm sure we can encapsulate this in a service, and just pass in
    // @TODO: the method, url and data, and pass stuff back, etc..
    $scope.registerUser = function registerUser() {
        // Do some basic validation.
        $scope.errorMessage = [];
        if ($scope.password != $scope.confirm_password) {
            $scope.errorMessage = ['Passwords must match'];
        } else {
            $http({
                method: "post",
                url: "api/v1/user",
                data: {
                    username: $scope.username,
                    password: $scope.password,
                    confirm_password: $scope.confirm_password
                }
            }).then(
                function success(response) {
                    if (response.data.success) {
                        // Registration was successful. Log the user in, then redirect
                        // to the projects list.
                        UsersApi
                            .login($scope.username, $scope.password)
                            .then(function() {
                                alert('Registration was successful');
                                $location.path('/projects');
                            })
                    } else {
                        $scope.errorMessage = response.data.errors;
                    }
                },
                function failure(response) {
                    console.log('Failure response: ', response);
                }
            );
        }
    };
    
});
