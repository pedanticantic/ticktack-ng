ticktack.controller('TicketViewCtrl', ['$scope', '$http', '$stateParams', '$location', '$timeout', '$injector', 'referenceData', 'allProjects', 'allUsers', 'ticketModel', 'assignmentModel', 'relationshipModel', 'commentModel', 'timeSpentModel', function ($scope, $http, $stateParams, $location, $timeout, $injector, referenceData, allProjects, allUsers, ticketModel, assignmentModel, relationshipModel, commentModel, timeSpentModel) {
    
    // Make sure we have all the projects and all the users.
    allProjects.refresh();
    allUsers.refresh();
    
    // This controller is used for viewing a ticket. It is now also used for creating a new ticket
    $scope.ticket_id = $stateParams.hasOwnProperty('id') ? $stateParams.id : null;
    $scope.ticket = null;
    $scope.editingTicket = false;
    
    $scope.adding = {
        assignment: false,
        relationship: false,
        comment: false,
        timeSpent: false
    };
    // A spinner is shown when any of these values is greater than 1.
    $scope.spinners = {
        ticket: 0,
        assignment: 0,
        relationship: 0,
        comment: 0,
        timeSpent: 0
    };
    
    $scope.staticData = {
        types: referenceData.getCodes('ticket_types'),
        priorities: referenceData.getCodes('ticket_priorities'),
        statuses: referenceData.getCodes('ticket_statuses')
    }
    
    $scope.projectData = {};
    $scope.userData = {};
    
    // @TODO: These codes need to come from the database.
    $scope.relationshipTypes = [
        {code: "associated", description: "associated with"},
        {code: "dependent", description: "a dependency of"},
        {code: "dependency", description: "dependent upon"},
        {code: "parent", description: "a child of"},
        {code: "child", description: "a parent of"}
    ];
    $scope.newRelationship = {
        relType: null,
        trgTicket: null
    };
    // Model for a new comment.
    $scope.newComment = {
        detail: null
    };
    // Model for a new time spent record.
    $scope.newTimeSpent = {
        startDatetime: null,
        duration: null,
        notes: null
    };
    
    /**
     * Method to load the ticket and associated data, or just one piece of associated data,
     * into the controller.
     * @param string whichPart Which part of the data to load; pass null to load everything.
     */
    $scope.reloadTicketData = function reloadTicketData(whichPart) {
        // If we have a ticket, load its data.
        if ($scope.ticket_id != null) {
            // Call the API endpoint to get all the data.
            $http({
                method: "get",
                url: "api/v1/ticket/" + $scope.ticket_id,
            }).then(
                function success(response) {
                    // Now set the model appropriate to which part we are loading.
                    // If the data has not been loaded at all, override the parameter and assume "all".
                    var objectTranslator = new ticktack.ObjectTranslator();
                    if ($scope.ticket == null) {
                        $scope.ticket = objectTranslator.translate(response.data.tickets[0], new ticketModel());
                    } else {
                        switch (whichPart) {
                            case 'assignments':
                                $scope.ticket.assignments = objectTranslator.translateArray(response.data.tickets[0].assignments, function() { return new assignmentModel(); });
                                break;
                            case 'associated':
                                $scope.ticket.associated = objectTranslator.translateArray(response.data.tickets[0].associated, function() { return new relationshipModel(); });
                                break;
                            case 'comments':
                                $scope.ticket.comments = objectTranslator.translateArray(response.data.tickets[0].comments, function() { return new commentModel(); });
                                break;
                            case 'timeSpent':
                                $scope.ticket.timesSpent = objectTranslator.translateArray(response.data.tickets[0].timesSpent, function() { return new timeSpentModel(); });
                                // Update the totals in the ticket model.
                                objectTranslator.translate(response.data.tickets[0], $scope.ticket);
                                break;
                            default:
                                var objectTranslator = new ticktack.ObjectTranslator();
                                $scope.ticket = objectTranslator.translate(response.data.tickets[0], new ticketModel());
                                break;
                        }
                    }
                    // We seem to need to do this because the checkbox requires a true/false value.
                    for (var aidx = 0 ; aidx < $scope.ticket.associated.length ; aidx++ ) {
                        $scope.ticket.associated[aidx].pinned = ($scope.ticket.associated[aidx].pinned == '1');
                    }
console.log('Time spent object:', $scope.ticket.timeSpentTotals);
                },
                function failure(response) {
                    alert('Failed to load the ticket. Click OK to see the list of tickets');
                    $location.path('/tickets');
                }
            );
        }
    }
    
    // Method to load all the tickets in the system, that the logged-in user can see.
    // @TODO: This needs to be done as a service.
    $scope.loadAllTickets = function loadAllTickets() {
        $http({
            method: "get",
            url: "api/v1/ticket",
        }).then(
            function success(response) {
                if (response.data.success) {
                    var objectTranslator = new ticktack.ObjectTranslator();
                    $scope.allTickets = objectTranslator.translateArray(response.data.tickets, function() { return new ticketModel(); });
                }
            },
            function failure(response) {
                alert('Failed to load all tickets.');
            }
        );
    }
    
    $scope.startWork = function startWork(whichSection) {
        if ($scope.spinners.hasOwnProperty(whichSection)) {
            $scope.spinners[whichSection]++;
        }
    }
    
    $scope.finishWork = function startWork(whichSection) {
        if ($scope.spinners.hasOwnProperty(whichSection)) {
            if ($scope.spinners[whichSection] > 0) {
                $scope.spinners[whichSection]--;
            }
        }
    }
    
    $scope.toggle = function toggle(which) {
        $scope.adding[which] = !$scope.adding[which];
        if (which == 'assignment') {
            $scope.populateAssigneeList();
        }
        if (which == 'timeSpent') {
            var tmpDate = new Date();
            $scope.newTimeSpent.startDatetime = tmpDate.toLocaleDateString() + ' ' + tmpDate.toLocaleTimeString();
            $scope.newTimeSpent.startDatetime = $scope.newTimeSpent.startDatetime.substr(0, 16);
        }
    }
    
    $scope.populateAssigneeList = function populateAssigneeList() {
        $scope.assignees = allUsers.getAllUsersData();
        if ($scope.assignees == null) {
            $scope.assignees = [];
        }
        // For some reason, it remembers this dummy row, so we have to check it's not there before we add it.
        if ($scope.assignees.length == 0 || $scope.assignees[0].id != -1) {
            $scope.assignees.unshift({
                id: -1,
                userName: 'Please select...'
            });
        }
    }
    
    $scope.populateAssigneeList();
    $scope.newAssignee = {id: $scope.assignees[0].id};
    
    // Assignments
    
    // User wants to create a new assignment.
    $scope.saveAssignment = function saveAssignment() {
        if ($scope.newAssignee.id > 0) {
            $scope.startWork('assignment');
            // Send the new assignment to the server.
            $http({
                method: "post",
                url: "api/v1/ticket/" + $scope.ticket_id + '/assignment/' + $scope.newAssignee.id,
            }).then(
                function success(response) {
                    if (response.data.success) {
                        // Assignment was successful. Load all the assignments back into memory.
                        $scope.reloadTicketData('assignments');
                        // Set the selection to the blank option and close the form.
                        $scope.newAssignee.id = $scope.assignees[0].id;
                        $scope.toggle('assignment');
                    }
                    $scope.finishWork('assignment');
                },
                function failure(response) {
                    alert('There was a problem saving this assignment: ' + response.data.message);
                    $scope.finishWork('assignment');
                }
            );
        }
    }
    
    $scope.unassign = function unassign(assignmentId) {
        var confirmDelete = confirm('Are you sure you want to remove this assignment?');
        if (!confirmDelete) {
            return;
        }
        $scope.startWork('assignment');
        $http({
            method: 'delete',
            url: 'api/v1/assignment/' + assignmentId
        }).then(
            function success(response) {
                if (response.data.success) {
                    // Delete was successul. Load all the assignments back into memory.
                    $scope.reloadTicketData('assignments');
                }
                $scope.finishWork('assignment');
            },
            function failure(response) {
                $scope.finishWork('assignment');
            }
        )
    }
    
    $scope.moveAssignment = function moveAssignment(assignmentId, dir) {
        if (dir == 'up' || dir == 'down') {
            $scope.startWork('assignment');
            $http({
                method: 'put',
                url: 'api/v1/assignment/' + assignmentId + '/' + dir
            }).then(
                function success(response) {
                    if (response.data.success) {
                        // Movement was successul. Load all the assignments back into memory.
                        $scope.reloadTicketData('assignments');
                    }
                    $scope.finishWork('assignment');
                },
                function failure(response) {
                    console.log('Failure. Response...', response);
                    $scope.finishWork('assignment');
                }
            )
        } else {
            alert('Unknown direction: ' + dir);
        }
    }
    
    // Relationships
    
    // Adding a new association.
    $scope.saveRelationship = function saveRelationship() {
        if ($scope.newRelationship.relType == null) {
            alert('You must choose a relationship type');
            return false;
        }
        if ($scope.newRelationship.trgTicket == null) {
            alert('You must choose a ticket');
            return false;
        }
        $scope.startWork('relationship');
        // Send the new relationship to the server.
        $http({
            method: "post",
            url: 'api/v1/ticket/' + $scope.ticket_id + '/associated',
            data: {
                relationship_type: $scope.newRelationship.relType.code,
                target_ticket_id: $scope.newRelationship.trgTicket.id,
                is_pinned: false
            }
        }).then(
            function success(response) {
                if (response.data.success) {
                    // Save was successful. Load all the relationships back into memory.
                    $scope.reloadTicketData('relationships');
                    // Set the selection to the blank options and close the form.
                    $scope.newRelationship.relType = null;
                    $scope.newRelationship.trgTicket = null;
                    $scope.toggle('relationship');
                }
                $scope.finishWork('relationship');
            },
            function failure(response) {
                alert('There was a problem saving this relationship: ' + response.data.message);
                $scope.finishWork('relationship');
            }
        );
    }
    
    // Toggle the pinned state of a relationship.
    $scope.togglePinned = function togglePinned(associationRow) {
        $scope.startWork('relationship');
        var pinAction = associationRow.pinned ? 'pin' : 'unpin';
        $http({
            method: 'put',
            url: 'api/v1/ticket/' + $scope.ticket.id + '/associated/' + associationRow.id + '/' + pinAction
        }).then(
            function success(response) {
                if (response.data.success) {
                    // Pin/unpin was successul. Load all the associations back into memory.
                    $scope.reloadTicketData('associated');
                }
                $scope.finishWork('relationship');
            },
            function failure(response) {
                $scope.finishWork('relationship');
            }
        )
    }
    
    // Move an association up/down.
    $scope.moveAssociated = function moveAssociated(associatedId, dir) {
        if (dir == 'up' || dir == 'down') {
            $scope.startWork('relationship');
            $http({
                method: 'put',
                url: 'api/v1/ticket/' + $scope.ticket.id + '/associated/' + associatedId + '/' + dir
            }).then(
                function success(response) {
                    if (response.data.success) {
                        // Movement was successul. Load all the associations back into memory.
                        $scope.reloadTicketData('associated');
                    }
                    $scope.finishWork('relationship');
                },
                function failure(response) {
                    $scope.finishWork('relationship');
                }
            )
        } else {
            alert('Unknown direction: ' + dir);
        }
    }
    
    // Delete an association.
    $scope.deleteAssociated = function deleteAssociated(associatedId) {
        if (confirm('Are you sure you want to delete this relationship?')) {
            $scope.startWork('relationship');
            $http({
                method: 'delete',
                url: 'api/v1/ticket/' + $scope.ticket.id + '/associated/' + associatedId
            }).then(
                function success(response) {
                    if (response.data.success) {
                        // Delete was successul. Load all the associations back into memory.
                        $scope.reloadTicketData('associated');
                    }
                    $scope.finishWork('relationship');
                },
                function failure(response) {
                    $scope.finishWork('relationship');
                }
            )
        }
    }
    
    // Convert the relationship type to a description.
    // @TODO: When the relationship types come from the DB, this method should be inside the Relationship model.
    $scope.relationshipTypeToDesc = function relationshipTypeToDesc(relType) {
        for(var relIndex in $scope.relationshipTypes) {
            if ($scope.relationshipTypes[relIndex].code == relType) {
                return $scope.relationshipTypes[relIndex].description;
            }
        }
        return '[Unknown]';
    }
    
    // Comments
    
    // Method to save a comment.
    $scope.saveComment = function saveComment() {
        if ($scope.newComment.detail) {
            $scope.startWork('comment');
            // Send the new comment to the server.
            $http({
                method: "post",
                url: 'api/v1/ticket/' + $scope.ticket_id + '/comment',
                data: {
                    details: $scope.newComment.detail
                    // "Added by" will default to the logged-in user.
                }
            }).then(
                function success(response) {
                    if (response.data.success) {
                        // Save was successful. Load all the comments back into memory.
                        $scope.reloadTicketData('comments');
                        // Set the selection to the blank options and close the form.
                        $scope.newComment.detail = null;
                        $scope.toggle('comment');
                    }
                    $scope.finishWork('comment');
                },
                function failure(response) {
                    alert('There was a problem saving your comment: ' + response.data.message);
                    $scope.finishWork('comment');
                }
            );
        } else {
            alert('You must actually enter a comment!');
        }
    }
    
    // Time Spent records.
    
    // Method to save a new time spent record.
    $scope.saveTimeSpent = function saveTimeSpent() {
        if ($scope.newTimeSpent.startDatetime &&
            $scope.newTimeSpent.duration &&
            $scope.newTimeSpent.notes) {
            $scope.startWork('timeSpent');
            // Send the new time spent record to the server.
            $http({
                method: "post",
                url: 'api/v1/ticket/' + $scope.ticket_id + '/time_spent',
                data: {
                    start_datetime: $scope.newTimeSpent.startDatetime,
                    duration: $scope.newTimeSpent.duration,
                    notes: $scope.newTimeSpent.notes
                    // "User" will default to the logged-in user.
                }
            }).then(
                function success(response) {
                    if (response.data.success) {
                        // Save was successful. Load all the time spent records back into memory.
                        $scope.reloadTicketData('timeSpent');
                        // Set the selection to the blank options and close the form.
                        $scope.newTimeSpent.startDatetime = null;
                        $scope.newTimeSpent.duration = null;
                        $scope.newTimeSpent.notes = null;
                        $scope.toggle('timeSpent');
                    }
                    $scope.finishWork('timeSpent');
                },
                function failure(response) {
                    alert('There was a problem recording your time: ' + response.data.message);
                    $scope.finishWork('timeSpent');
                }
            );
        } else {
            alert('You must enter the start time, duration and notes!');
        }
    }
    
    // Ticket editing methods.
    $scope.toggleEdit = function toggleEdit(newState) {
        if (newState == undefined) {
            $scope.editingTicket = !$scope.editingTicket;
        } else {
            $scope.editingTicket = newState;
        }
        if ($scope.editingTicket) {
            // We're editing a ticket - make sure the project- and user lists are up to date.
            $scope.projectData = allProjects.getAllProjectsData();
            $scope.userData = allUsers.getAllUsersData();
        } else {
            if ($scope.ticket_id == null) {
                // They've canceled an insert, so jump back to the ticket list.
                $location.path('/tickets');
            }
        }
    }
    
    if ($scope.ticket_id == null) {
        $timeout(function() {
            $scope.projectData = allProjects.getAllProjectsData();
            $scope.projectData.unshift({
                id: -1,
                name: 'Please select...'
            });
            $scope.userData = allUsers.getAllUsersData();
            // If created by user is not set, find out who is currently logged in, and set it to that.
            if ($scope.ticket.created_by_user_id == null) {
                var userSession = $injector.get('LoggedInUser');
                $scope.ticket.setCreatedByUserId(userSession.getLoggedInUserId($scope.userData));
                // If there's still no selection, pick the first one!
                if ($scope.ticket.getCreatedByUserId() == null) {
                    $scope.ticket.setCreatedByUserId($scope.userData[0].id);
                }
            }
        }, 1000);
        $scope.ticket = new ticketModel();
        $scope.toggleEdit();
    }
    
    // Save the ticket.
    $scope.saveTicket = function saveTicket() {
        // @TODO: Do any validation here.
        // Save the ticket.
        $scope.startWork('ticket');
        var saveMethod = $scope.ticket_id == null ? 'post' : 'put';
        var saveUrl = 'api/v1/ticket' + ($scope.ticket_id == null ? '' : '/' + $scope.ticket.id);
        $http({
            method: saveMethod,
            url: saveUrl,
            data: {
                // @TODO: Do We need getters for all these fields?
                project_id: $scope.ticket.projectId,
                ticket_type: $scope.ticket.ticketType,
                summary: $scope.ticket.summary,
                description: $scope.ticket.description,
                created_by_user_id: $scope.ticket.createdByUserId,
                priority: $scope.ticket.priority,
                status: $scope.ticket.status,
                due_datetime: $scope.ticket.dueDatetimeForView,
                estimated_length_seconds: $scope.ticket.estimatedLengthForView
            }
        }).then(
            function success(response) {
                if (response.data.success) {
                    // Save ticket was successul. Load the ticket back into memory.
                    if ($scope.ticket_id == null) {
                        $location.path('tickets/' + response.data.id);
                    } else {
                        $scope.reloadTicketData('ticket');
                        $scope.toggleEdit();
                    }
                }
                $scope.finishWork('ticket');
            },
            function failure(response) {
                alert(response.data.message);
                $scope.finishWork('ticket');
            }
        )
    }
    
    // Load all the data for this ticket.
    $scope.reloadTicketData();
    
    // Load the basic info for all tickets.
    // @TODO: This needs to be a service that runs once at startup, and maybe periodically (or after tickets are edited).
    $scope.loadAllTickets();
    
    $scope.codeToDesc = function codeToDesc(refType, refValue) {
        return referenceData.getDesc(refType, refValue);
    }
    
}]);
