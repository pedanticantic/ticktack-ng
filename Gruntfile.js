module.exports = function(grunt) {
    
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json')
    });
    
    // Project configuration.
    // I have split out the "framework" or "resources" files from my application files because the former
    // are quite big and hardly ever change, whereas the app files change all the time. We build 2
    // separate minified files - one for the resources and one for the app, and finally just join them
    // together.
    grunt.initConfig({
        concat: {
            // Common options.
            options: {
                separator: "\n\n",
            },
            // Concatenate the files for the framework - Angular and the modules.
            res: {
                src: [
                    'bower_components/jquery/dist/jquery.min.js',
                    'bower_components/angular/angular.min.js',
                    'bower_components/angular-ui-router/release/angular-ui-router.min.js',
                    'bower_components/angular-resource/angular-resource.min.js',
                    'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
                    'bower_components/bootstrap/dist/js/bootstrap.min.js'
                ],
                dest: 'app/built/resources.js',
            },
            // Concatenate the files for the application. Ie my code.
            app: {
                src: [
                    'app/app.js',
                    'app/models/*.js',
                    'app/routes/routes.js',
                    'app/services/*.js',
                    'app/controllers/*.js'
                ],
                dest: 'app/built/application.js',
            },
            // Concatenate the resources and application code into the final file.
            full: {
                src: [
                    'app/built/resources.min.js',
                    'app/built/templates.js',
                    'app/built/application.min.js'
                ],
                dest: 'app/built/ticktack.min.js',
            }
        },
        
        uglify: {
            // Common options.
            options: {
                mangle: false,
                banner: "/* TickTack - generated <%= grunt.template.today('yyyy-mm-dd HH:MM:ss') %> */\n",
                screwIE8: true
            },
            app: {
                files: {
                    'app/built/application.min.js': ['app/built/application.js']
                }
            },
            res: {
                files: {
                    'app/built/resources.min.js': ['app/built/resources.js']
                }
            }
        },
        
        html2js: {
            options: {
                base: 'app',
                indentString: "\t",
                useStrict: true
            },
            main: {
                src: 'app/partials/*.html',
                dest: 'app/built/templates.js'
            }
        },
        
        watch: {
            build: {
                files: [
                    'app/models/*.js',
                    'app/controllers/*.js',
                    'app/routes/*.js',
                    'app/services/*.js',
                    'app/*.js'
                ],
                tasks: [
                    'default'
                ]
            },
            build_res: {
                files: [
                    'bower_components/jquery/dist/jquery.min.js',
                    'bower_components/angular/angular.min.js',
                    'bower_components/angular-ui-router/release/angular-ui-router.min.js',
                    'bower_components/angular-resource/angular-resource.min.js',
                    'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
                    'bower_components/bootstrap/dist/js/bootstrap.min.js'
                ],
                tasks: [
                    'build_res',
                    'build'
                ]
            },
            build_templates: {
                files: [
                    'app/partials/*.html'
                ],
                tasks: [
                    'html2js',
                    'build'
                ]
            }
        }
    });
    
    // Load the various plugins.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-html2js');
    
    // Default task
    grunt.registerTask('default', ['build']);
    
    /**
     * Build the resources task.
     */
    grunt.registerTask('build_res', function () {
        grunt.task.run([
            'concat:res',
            'uglify:res'
        ]);
    });
    
    /**
     * Build the templates task.
     */
    grunt.registerTask('build_template', function () {
        grunt.task.run([
            'html2js'
        ]);
    });
    
    /**
     * Build everything task.
     */
    grunt.registerTask('build', function () {
        grunt.task.run([
            'concat:app',
            'uglify:app',
            'concat:full'
        ]);
    });
    
};